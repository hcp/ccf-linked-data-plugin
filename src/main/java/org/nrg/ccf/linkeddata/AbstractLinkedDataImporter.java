package org.nrg.ccf.linkeddata;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.comparators.FileNameComparator;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoXnatExperimentdata;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.zip.TarUtils;
import org.nrg.xft.utils.zip.ZipI;
import org.nrg.xft.utils.zip.ZipUtils;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectExptResourceImpl;
import org.nrg.xnat.helpers.resource.direct.DirectScanResourceImpl;
import org.nrg.xnat.restlet.actions.importer.ImporterHandlerA;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.python.google.common.collect.Lists;

/**
 * The Class AbstractLinkedDataImporter.
 */
public abstract class AbstractLinkedDataImporter extends ImporterHandlerA implements Callable<List<String>> {

	/** The listener control. */
	private Object listenerControl;
	
	/** The user. */
	protected UserI user;
	
	/** The fw. */
	private FileWriterWrapperI fw;
	
	/** The params. */
	protected Map<String, Object> params;
	
	/** The return list. */
	private final List<String> returnList = Lists.newArrayList();
	
	protected XnatSubjectdata subject;
	
	/** The proj. */
	protected XnatProjectdata proj;
	
	/** The exp. */
	protected XnatImagesessiondata exp;
	
	/** The scan modifiers. */
	private Map<String,DirectScanResourceImpl> scanModifiers = new HashMap<String,DirectScanResourceImpl>(); 
	
	/** The session modifier. */
	private DirectExptResourceImpl sessionModifier;
	
	/** The modified scans. */
	final Map<XnatImagescandataI,List<String>> modifiedScans = new HashMap<>();
	
	/** The linked resource label. */
	public String LINKED_RESOURCE_LABEL="LINKED_DATA";
	
	/** The linked resource format. */
	public String LINKED_RESOURCE_FORMAT="MISC";
	
	/** The linked resource content. */
	public String LINKED_RESOURCE_CONTENT="RAW";
	
	/** The linked catxml prefix. */
	public String LINKED_CATXML_PREFIX="linkeddata_";
	
	/** The linked catxml ext. */
	public String LINKED_CATXML_EXT="_catalog.xml";
	
	/** The event action. */
	public String EVENT_ACTION="Linked-data upload";
	
	/** The logger. */
	static Logger logger = Logger.getLogger(AbstractLinkedDataImporter.class);
	
	/** The Constant ZIP_EXT. */
	static final String[] ZIP_EXT={".zip",".jar",".rar",".ear",".gar",".xar",".tar",".tar.gz",".tgz"};
	
	/**
	 * Instantiates a new abstract linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public AbstractLinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u);
		this.listenerControl = listenerControl;
		this.user = u;
		this.fw = fw;
		this.params = params;
	}

	/**
	 * Instantiates a new abstract linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public AbstractLinkedDataImporter(UserI u, Map<String, Object> params) {
		this(null, u, null, params);
	}

	/* (non-Javadoc)
	 * @see org.nrg.xnat.restlet.actions.importer.ImporterHandlerA#call()
	 */
	@Override
	public List<String> call() throws ClientException, ServerException {
		if (this.fw == null || this.listenerControl == null) {
			return callFromAutomationUploader();
		}
		verifyAndGetExperiment();
		try {
			processUpload();
			this.completed("Success");
			return returnList;
		} catch (ClientException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			this.failed(e.getMessage());
			returnList.add("<br>" + e.toString());
			throw e;
		} catch (ServerException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			this.failed(e.getMessage());
			returnList.add("<br>" + e.toString());
			throw e;
		} catch (Throwable e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			returnList.add("<br>" + e.toString());
			throw new ServerException(e.getMessage(),new Exception());
		}
	}
	
	public void sendNotifications(final String[] emailAddrs) {
		AdminUtils.sendUserHTMLEmail(exp.getProjectDisplayID() +" - LinkedDataUploader results (SESSION=" + exp.getLabel() + ")", returnListToHtmlString(), false, emailAddrs);
	}

	/**
	 * Call from automation uploader.
	 *
	 * @return the list
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	public List<String> callFromAutomationUploader() throws ClientException, ServerException {
		if (!this.params.containsKey("BuildPath")) {
			throw new ClientException("ERROR:  BuildPath was not specified");
		}
		verifyAndGetExperiment();
		try {
			processCacheFiles(new File(this.params.get("BuildPath").toString()));
			return returnList;
		} catch (ClientException e) {
			logger.error("",e);
			returnList.add("<br>" + e.toString());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			returnList.add("<br>" + e.toString());
			throw e;
		} catch (Throwable e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			returnList.add("<br>" + e.toString());
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	/**
	 * Verify and get experiment.
	 *
	 * @throws ClientException the client exception
	 */
	private void verifyAndGetExperiment() throws ClientException {
		String projID=null;
		if (params.get("project")!=null) {
			projID=params.get("project").toString();
		}
		String subjLbl=null;
		if (params.get("subject")!=null) {
			subjLbl=params.get("subject").toString();
		}
		if (params.get("experiment") == null) {
			clientFailed("ERROR:  experiment parameter (containing experiment label) must be supplied for import");
		}
		String expLbl=params.get("experiment").toString();
		// NOTE:  This call failed when calling from a script when setting the preload parameter to false.  It worked okay from inside the session.
		this.exp =XnatMrsessiondata.getXnatMrsessiondatasById(expLbl, user, true);
		if(exp==null){
			// NOTE:  Also setting preload parameter to true here.  Likely otherwise wouldn't work from scripts.
			final ArrayList<XnatMrsessiondata> al=XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData/label",expLbl, user, true);
			// Using iterator here because removing within for/next can be unpredictable
			final Iterator<XnatMrsessiondata> aIter = al.iterator();
			while (aIter.hasNext()) {
				final XnatMrsessiondata mrsess = aIter.next();
				if (projID!=null && !mrsess.getProject().equalsIgnoreCase(projID)) {
					aIter.remove();
					continue;
				}
				if (subjLbl!=null && 
						!(mrsess.getSubjectData().getId().equalsIgnoreCase(subjLbl) || mrsess.getSubjectData().getLabel().equalsIgnoreCase(subjLbl))) {
					aIter.remove();
					continue;
				}
				exp=mrsess;
			}
			if (al.size()>1) {
				clientFailed("ERROR:  Multiple sessions match passed parameters. Consider using experiment assession number or supplying project/subject parameters ");
			}
		}
			if (exp==null) {
			clientFailed("ERROR:  Could not find matching experiment for import");
		}
			proj=exp.getProjectData();
	}
	

	/**
	 * Client failed.
	 *
	 * @param fmsg the fmsg
	 * @throws ClientException the client exception
	 */
	//@SuppressWarnings("deprecation")
	protected void clientFailed(final String fmsg) throws ClientException {
		//this.failed(fmsg);
		throw new ClientException(fmsg,new Exception());
	}
	

	/**
	 * Process upload.
	 *
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	private void processUpload() throws ClientException,ServerException {
		
		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		
		boolean doProcess = false;
		// Multiple uploads are allowed to same space (processing will take place when process parameter=true).  Use specified build path when
		// one is given, otherwise create new one
		String specPath=null;
		boolean invalidSpecpath = false;
		if (params.get("buildPath")!=null) {
			// If buildpath parameter is specified and valid, use it
			specPath=params.get("buildPath").toString();
			if (specPath.indexOf(cachePath)>=0 && specPath.indexOf("user_uploads")>=0 &&
					specPath.indexOf(File.separator + user.getID() + File.separator)>=0 && new File(specPath).isDirectory()) {
				cachePath=specPath;
			} else {
				specPath=null;
				invalidSpecpath = true;
			}
		} 
		if (specPath==null) {
			final Date d = Calendar.getInstance().getTime();
			final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
			final String uploadID = formatter.format(d);
			// Save input files to cache space
			cachePath+="user_uploads/"+user.getID() + "/" + uploadID + "/";
		}
		// If uploading zip file with no process or build directory specified, will proceed with processing regardless unless told not 
		// to via process parameter.  Otherwise will only process when told to (when done uploading).
		String processParm = null;
		if (params.get("process")!=null) {
			processParm = params.get("process").toString();
		}
		
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
		
		// If uploading a file, process it.  Otherwise just set doProcess parameter (default=true)
		if (fw!=null) {
			doProcess=processFile(cacheLoc, specPath, processParm, doProcess, invalidSpecpath);
		} else {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		}
		
		// Conditionally process cache location files, otherwise return cache location
		if (doProcess) {
			processCacheFiles(cacheLoc);
			if (params.get("sendemail")!=null && params.get("sendemail").toString().equalsIgnoreCase("true")) {
				sendUserEmail(true);
			} else {
				sendAdminEmail();
			}
		} else {
			returnList.add(cachePath);
		}
		
	}
	
	/**
	 * Return list to html string.
	 *
	 * @return the string
	 */
	private String returnListToHtmlString() {
		final StringBuilder sb = new StringBuilder("<br>");
		
		for (ListIterator<String> iter = returnList.listIterator(); iter.hasNext(); ) {
			final String s = iter.next();
			sb.append(s);
			if (iter.hasNext()) {
				sb.append("<br>\t");
			}
		}
		return sb.toString();
	}

	/**
	 * Send user email.
	 *
	 * @param ccAdmin the cc admin
	 */
	private void sendUserEmail(boolean ccAdmin) {
			AdminUtils.sendUserHTMLEmail(exp.getProjectDisplayID() +" - LinkedDataUploader results (SESSION=" + exp.getLabel() + ")", returnListToHtmlString(), ccAdmin, new String[] { user.getEmail() });
	}
	
	/**
	 * Send admin email.
	 */
	private void sendAdminEmail() {
			AdminUtils.sendAdminEmail(user,exp.getProjectDisplayID() +" - LinkedDataUploader results (SESSION=" + exp.getLabel() + ")", returnListToHtmlString());
	}

	/**
	 * Process file.
	 *
	 * @param cacheLoc the cache loc
	 * @param specPath the spec path
	 * @param processParm the process parm
	 * @param doProcess the do process
	 * @param invalidSpecpath the invalid specpath
	 * @return true, if successful
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 */
	private boolean processFile(final File cacheLoc,final  String specPath,final  String processParm,boolean doProcess,final  boolean invalidSpecpath) throws ServerException, ClientException {
		final String fileName = fw.getName();
		if (specPath==null && isZipFileName(fileName)) {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		// If not uploading a zip file, only process when told to (via processParm) 
		} else if (!isZipFileName(fileName)) {
			if (processParm!=null && (processParm.equalsIgnoreCase("YES") || processParm.equalsIgnoreCase("TRUE"))) {
				doProcess=true;
			}
		}
		
		if (invalidSpecpath && !doProcess) {
			throw new ClientException("ERROR:  Specified build path is invalid");
		}
		
		if (isZipFileName(fileName)) {
			final ZipI zipper = getZipper(fileName);
			try {
				zipper.extract(fw.getInputStream(),cacheLoc.getAbsolutePath());
			} catch (Exception e) {
				logger.error(ExceptionUtils.getStackTrace(e));
				throw new ClientException("ERROR:  Archive file is corrupt or not a valid archive file type.");
			}
		} else {
			final File cacheFile = new File(cacheLoc,fileName);
			try {
				FileUtils.copyInputStreamToFile(fw.getInputStream(), cacheFile);
			} catch (IOException e) {
				logger.error(ExceptionUtils.getStackTrace(e));
				throw new ServerException("ERROR:  Could not write uploaded file.");
			}
		}
		return doProcess;
	}
	

	/**
	 * Checks if is zip file name.
	 *
	 * @param fileName the file name
	 * @return true, if is zip file name
	 */
	private boolean isZipFileName(final String fileName) {
		for (final String ext : ZIP_EXT) {
			if (fileName.toLowerCase().endsWith(ext)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the zipper.
	 *
	 * @param fileName the file name
	 * @return the zipper
	 */
	private ZipI getZipper(final String fileName) {
		
		// Assume file name represents correct compression method
		String file_extension = null;
		if (fileName!=null && fileName.indexOf(".")!=-1) {
			file_extension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
			if (Arrays.asList(ZIP_EXT).contains(file_extension)) {
				return new ZipUtils();
			} else if (file_extension.equalsIgnoreCase(".tar")) {
				return new TarUtils();
			} else if (file_extension.equalsIgnoreCase(".gz") || file_extension.equalsIgnoreCase(".tgz")) {
				TarUtils zipper = new TarUtils();
				zipper.setCompressionMethod(ZipOutputStream.DEFLATED);
				return zipper;
			}
		}
		// Assume zip-compression for unnamed inbody files
		return new ZipUtils();
		
	}
	
	/**
	 * Gets the current project.
	 *
	 * @return the current project
	 */
	public XnatProjectdata getCurrentProject() {
		return this.proj;
	}
	
	/**
	 * Gets the current experiment.
	 *
	 * @return the current experiment
	 */
	public XnatExperimentdata getCurrentExperiment() {
		return this.exp;
	}
	
	/**
	 * Gets the return list.
	 *
	 * @return the return list
	 */
	public List<String> getReturnList() {
		return this.returnList;
	}
	
	
	/**
	 * Creates the resource.
	 *
	 * @param thisScan the this scan
	 * @param ci the ci
	 * @return true, if successful
	 * @throws ServerException the server exception
	 */
	private boolean createResource(AutoXnatImagescandata thisScan, EventMetaI ci) throws ServerException {
		
		final File scanDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + exp.getLabel() +
					File.separator + "SCANS" + File.separator + thisScan.getId() + File.separator + LINKED_RESOURCE_LABEL);
		if (!scanDir.exists()) {
			scanDir.mkdirs();
		}
		final File catFile = new File(scanDir,LINKED_CATXML_PREFIX + thisScan.getId() + LINKED_CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(LINKED_RESOURCE_LABEL);
		ecat.setFormat(LINKED_RESOURCE_FORMAT);
		ecat.setContent(LINKED_RESOURCE_CONTENT);
		// Save resource to scan
		try {
			thisScan.addFile(ecat);
			if (SaveItemHelper.authorizedSave(thisScan,user,false,true,ci)) {
				//final String eventStr = "Resource " + LINKED_RESOURCE_LABEL + " created under scan " + thisScan.getId() + " (" + thisScan.getSeriesDescription() + ")" ;
				//returnList.add(eventStr); 
				return true;
			};
			return false;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("ERROR:  Couldn't add resource to scan - " + e.getMessage(),new Exception());
		}
		
	}
	
	/**
	 * Creates the session resource if necessary.
	 *
	 * @param thisSession the this session
	 * @param sessionModifier the session modifier
	 * @param createIfNecessary the create if necessary
	 * @param ci the ci
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 */
	@SuppressWarnings("unused")
	private void createSessionResourceIfNecessary(final XnatExperimentdata thisSession,final DirectExptResourceImpl sessionModifier,
					final boolean createIfNecessary, final EventMetaI ci) throws ServerException, ClientException {

		final XnatAbstractresourceI resource =  sessionModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
		if (createIfNecessary && resource==null) {
			createSessionResource(thisSession, ci);
		}
		
	}
	
	/**
	 * Creates the session resource.
	 *
	 * @param thisSession the this session
	 * @param ci the ci
	 * @return true, if successful
	 * @throws ServerException the server exception
	 */
	private boolean createSessionResource(final AutoXnatExperimentdata thisSession, EventMetaI ci) throws ServerException {
		
		final File sessionDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(exp.getProjectData().getId()) + exp.getProjectData().getCurrentArc() + File.separator + exp.getLabel() +
					File.separator + "RESOURCES" + File.separator + LINKED_RESOURCE_LABEL);
		if (!sessionDir.exists()) {
			sessionDir.mkdirs();
		}
		final File catFile = new File(sessionDir,LINKED_CATXML_PREFIX + thisSession.getId() + LINKED_CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(LINKED_RESOURCE_LABEL);
		ecat.setFormat(LINKED_RESOURCE_FORMAT);
		ecat.setContent(LINKED_RESOURCE_CONTENT);
		// Save resource to session
		//final String eventStr = "Resource " + LINKED_RESOURCE_LABEL + " created under session " + thisSession.getId();
		try {
			thisSession.addResources_resource(ecat);
			if (SaveItemHelper.authorizedSave(thisSession,user,false,true,ci)) {
				//returnList.add(eventStr); 
				return true;
			} 
			return false;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("ERROR:  Couldn't add resource to session - " + e.getMessage(),new Exception());
		}
		
	}
	
	/**
	 * Creates the resource if necessary.
	 *
	 * @param sessionModifier the session modifier
	 * @param createIfNecessary the create if necessary
	 * @param ci the ci
	 * @throws ServerException the server exception
	 */
	private void createResourceIfNecessary(DirectExptResourceImpl sessionModifier, boolean createIfNecessary, EventMetaI ci) throws ServerException {

		final XnatAbstractresourceI resource =  sessionModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
		if (createIfNecessary && resource==null) {
			createSessionResource(exp, ci);
		}
		
	}
	
	/**
	 * Creates the resource if necessary.
	 *
	 * @param thisScan the this scan
	 * @param scanModifier the scan modifier
	 * @param createIfNecessary the create if necessary
	 * @param ci the ci
	 * @throws ServerException the server exception
	 */
	private void createResourceIfNecessary(XnatImagescandataI thisScan, DirectScanResourceImpl scanModifier, boolean createIfNecessary,
			EventMetaI ci) throws ServerException {

		final XnatAbstractresourceI resource =  scanModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
		if (createIfNecessary && resource==null) {
			createResource((XnatImagescandata)thisScan, ci);
		}
		
	}
	
	/**
	 * Upload file to scan.
	 *
	 * @param thisScan the this scan
	 * @param file the file
	 * @param type the type
	 * @param ci the ci
	 * @return true, if successful
	 * @throws ServerException the server exception
	 */
	protected boolean uploadFileToScan(final XnatImagescandataI thisScan,final File file,String type, EventMetaI ci) throws ServerException {
		
		final String fileCatPath = type + File.separator + file.getName();
		
		try {
			
			// See if file already exists
			boolean alreadyExists=doesExist(thisScan,file,type,fileCatPath);
			
			final DirectScanResourceImpl scanModifier = getScanModifier(thisScan,ci);
			createResourceIfNecessary(thisScan,scanModifier, true, ci);
			
			setScanModified(thisScan,type);
			
			// Clean count, size and files from resourcecatalog, otherwise stats will not be updated.
			final XnatAbstractresourceI resource = scanModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
			if (resource instanceof BaseXnatResourcecatalog) {
				((BaseXnatResourcecatalog)resource).clearCountAndSize();
				((BaseXnatResourcecatalog)resource).clearFiles();
			}
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			fws.add(new StoredFile(file,alreadyExists));
			try {
				@SuppressWarnings("unused")
				final List<String> duplicates = scanModifier.addFile(fws, LINKED_RESOURCE_LABEL, null,fileCatPath, new XnatResourceInfo(user,new Date(),new Date()), false);
				final String eventStr = ((alreadyExists) ? "Replaced existing " : "Uploaded ") + type + 
						" File (<b>" + file.getName() + "</b>) for scan " + thisScan.getId() +
						" (<b>" + thisScan.getSeriesDescription() + "</b>).";
				returnList.add(eventStr);
				return true;
			} catch(Exception e) {
				logger.error(ExceptionUtils.getStackTrace(e));
				returnList.add("WARNING:  Could not add " +  type + " File (<b>" + file.getName() + "</b> to scan " + thisScan.getId() + " (<b>" + thisScan.getSeriesDescription() + "</b>))");
				return false;
			}
			
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("ERROR:  Could not add " + type + " files to resource");
		}
		
	}
	
	/**
	 * Upload file to session.
	 *
	 * @param file the file
	 * @param type the type
	 * @param ci the ci
	 * @return true, if successful
	 * @throws ServerException the server exception
	 */
	protected boolean uploadFileToSession(final File file,String type, EventMetaI ci) throws ServerException {
		
		final String fileCatPath = type + File.separator + file.getName();
		
		try {
			
			// See if file already exists
			boolean alreadyExists=doesExist(exp, file, type, fileCatPath);
			
			final DirectExptResourceImpl sessionModifier = getSessionModifier(ci);
			createResourceIfNecessary(sessionModifier, true, ci);
			
			//setScanModified(thisScan,type);
			
			// Clean count, size and files from resourcecatalog, otherwise stats will not be updated.
			final XnatAbstractresourceI resource = sessionModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
			if (resource instanceof BaseXnatResourcecatalog) {
				((BaseXnatResourcecatalog)resource).clearCountAndSize();
				((BaseXnatResourcecatalog)resource).clearFiles();
			}
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			fws.add(new StoredFile(file,alreadyExists));
			try {
				@SuppressWarnings("unused")
				final List<String> duplicates = sessionModifier.addFile(fws, LINKED_RESOURCE_LABEL, null,fileCatPath, new XnatResourceInfo(user,new Date(),new Date()), false);
				final String eventStr = ((alreadyExists) ? "Replaced existing " : "Uploaded ") + type + 
						" File (<b>" + file.getName() + "</b>) at session level for session " + exp.getLabel() + ".";
				returnList.add(eventStr);
				return true;
			} catch(Exception e) {
				returnList.add("WARNING:  Could not add " +  type + " File (<b>" + file.getName() + "</b> to session level for session " + exp.getId() + ".");
				logger.error(ExceptionUtils.getStackTrace(e));
				return false;
			}
			
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			throw new ServerException("ERROR:  Could not add " + type + " files to resource");
		}
		
	}
	
	/**
	 * Sets the scan modified.
	 *
	 * @param scan the scan
	 * @param type the type
	 */
	private void setScanModified(XnatImagescandataI scan,String type) {
		if (!modifiedScans.containsKey(scan)) {
			modifiedScans.put(scan, new ArrayList<String>(Arrays.asList(type)));
		} else {
			final List<String> typeList = modifiedScans.get(scan);
			if (!typeList.contains(type)) {
				typeList.add(type);
			}
		} 
	}
	
	/**
	 * Does exist.
	 *
	 * @param thisSession the this session
	 * @param pFile the file
	 * @param type the type
	 * @param fileCatPath the file cat path
	 * @return true, if successful
	 */
	private boolean doesExist(XnatImagesessiondata thisSession, File pFile, String type, String fileCatPath) {
		final List<XnatAbstractresourceI> thisFL = thisSession.getResources_resource();
		return checkFileExist(thisFL,pFile,type,fileCatPath);
	}

	/**
	 * Does exist.
	 *
	 * @param thisScan the this scan
	 * @param file the file
	 * @param type the type
	 * @param fileCatPath the file cat path
	 * @return true, if successful
	 */
	private boolean doesExist(XnatImagescandataI thisScan, File file, String type, String fileCatPath) {
		final List<XnatAbstractresourceI> thisFL = thisScan.getFile();
		return checkFileExist(thisFL,file,type,fileCatPath);
	}
	
	/**
	 * Check file exist.
	 *
	 * @param thisFL the this fl
	 * @param pFile the file
	 * @param type the type
	 * @param fileCatPath the file cat path
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	private boolean checkFileExist(List<XnatAbstractresourceI> thisFL, File pFile, String type, String fileCatPath) {
		for (final XnatAbstractresourceI thisF : thisFL) {
			if (thisF instanceof XnatResourcecatalog) {
				if (thisF.getLabel().equals(LINKED_RESOURCE_LABEL)) {
					final XnatResourcecatalog thisC = (XnatResourcecatalog) thisF;
					final ArrayList<File> files = thisC.getCorrespondingFiles("/");
					for (final File f : files) {
						if (f.getPath().endsWith(fileCatPath)) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}

	/**
	 * Gets the session modifier.
	 *
	 * @param ci the ci
	 * @return the session modifier
	 */
	private DirectExptResourceImpl getSessionModifier(final EventMetaI ci) {
		return (sessionModifier!=null) ? sessionModifier : new DirectExptResourceImpl(proj,exp,true,user,ci);
	}

	/**
	 * Gets the scan modifier.
	 *
	 * @param scan the scan
	 * @param ci the ci
	 * @return the scan modifier
	 */
	protected DirectScanResourceImpl getScanModifier(final XnatImagescandataI scan,final EventMetaI ci) {
		
		String key=scan.getId()+"_"+scan.getImageSessionId();
		if (scanModifiers.containsKey(key)) { return
				scanModifiers.get(key); 
		} 
		else {
			final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl((XnatImagescandata)scan,exp,true,user,ci);
			scanModifiers.put(scan.getId()+"_"+scan.getImageSessionId(),scanModifier);
			return scanModifier;
		}
		/* if (scanModifiers.containsKey(scan.getId())) { return
		 scanModifiers.get(scan.getId()); 
		 } 
		 else {
			final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl((XnatImagescandata)scan,exp,true,user,ci);
			scanModifiers.put(scan.getId(),scanModifier);
			return scanModifier;
		}*/
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	protected UserI getUser() {
		return user;
	}
	
	/**
	 * Gets the project.
	 *
	 * @return the project
	 */
	protected XnatProjectdata getProject() {
		return proj;
	}
	
	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	protected XnatImagesessiondata getSession() {
		return exp;
	}
	
	/**
	 * Process cache files.
	 *
	 * @param cacheLoc the cache loc
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	public abstract void processCacheFiles(final File cacheLoc) throws ClientException, ServerException;
	
	/**
	 * Upload files to resources.
	 *
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 */
	public synchronized Boolean uploadFilesToResources(Map<File, LinkedFileInfo> uploadFileMap) throws ServerException, ClientException {
		
		PersistentWorkflowI wrk;
		Boolean upload_status = true;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), getSession().getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EVENT_ACTION, EVENT_ACTION, null));
			final EventMetaI ci = wrk.buildEvent();
			final List<File> keyList = new ArrayList<>();
			keyList.addAll(uploadFileMap.keySet());
			Collections.sort(keyList, new FileNameComparator());
			
			for (final File f : keyList) {
				final LinkedFileInfo fileInfo = uploadFileMap.get(f);
				if (fileInfo != null) {
					if (fileInfo.getScan() == null) {
						continue;
					}
					try {
						// Upload scan files
						if(!getReturnList().contains(Constants.UPLOAD_SCAN_LEVEL_RESOURCES_MESSAGE))
							getReturnList().add(Constants.UPLOAD_SCAN_LEVEL_RESOURCES_MESSAGE);
						//logger.debug("--->"+fileInfo.getScan()+"--->"+fileInfo.getFileType());
						final boolean status = uploadFileToScan(fileInfo.getScan(), f, fileInfo.getFileType(), ci);
						System.out.println("Status :: "+status);
						upload_status = (!status) ? status : upload_status; 
					} catch (ServerException e) {
						getReturnList().add(MessageFormat.format(Constants.SCAN_FILE_UPLOAD_ERROR_MESSAGE, f.getName()));
					}
				}
			}
			int count=0;
			for (final File f : keyList) {
				final LinkedFileInfo fileInfo = uploadFileMap.get(f);
				if (fileInfo != null) {
					if (fileInfo.getScan() != null) {
						continue;
					}
					try {
						count++;
						if(count==1)
							getReturnList().add(Constants.UPLOAD_SESSION_LEVEL_RESOURCES_MESSAGE);
						final boolean status = uploadFileToSession(f, fileInfo.getFileType(), ci);
						upload_status = (!status) ? status : upload_status; 
					} catch (ServerException e) {
						getReturnList().add(MessageFormat.format(Constants.SESSION_FILE_UPLOAD_ERROR_MESSAGE, f.getName()));
					}
				}
			}
			if (upload_status) {
				PersistentWorkflowUtils.complete(wrk, ci);
				//getReturnList().add(Constants.FILE_UPLOAD_SUCCESS_MESSAGE);
			} else {
				PersistentWorkflowUtils.fail(wrk, ci);
				//getReturnList().add(Constants.FILE_UPLOAD_ERROR_MESSAGE);
			}
		} catch (Exception e1) {
			logger.error(ExceptionUtils.getStackTrace(e1));
			upload_status=false;
			throw new ServerException(MessageFormat.format(Constants.ERROR_FAILED_FILES_MESSAGE,e1.toString()));
		}
		return upload_status;
	}
	
}
