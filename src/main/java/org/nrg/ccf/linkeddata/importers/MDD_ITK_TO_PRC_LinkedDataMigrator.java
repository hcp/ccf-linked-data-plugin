package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.DirectoryFilter;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.resource.direct.DirectScanResourceImpl;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.FileSystemUtils;

import com.google.common.collect.Maps;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;


/**
 * The Class MDD_ITK_TO_PRC_LinkedDataMigrator.
 *
 * @author Atul
 */
public class MDD_ITK_TO_PRC_LinkedDataMigrator extends AbstractLinkedDataImporter {

	/** The Constant JAVA_IO_TMPDIR. */
	private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";

	/** The logger. */
	static Logger logger = Logger.getLogger(MDD_ITK_TO_PRC_LinkedDataMigrator.class);

	/** The upload file map. */
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The Constant QUERY_IMG_SESSION_IDS. */
	private static final String QUERY_IMG_SESSION_IDS = "SELECT DISTINCT isd.id FROM xnat_imageSessionData isd LEFT JOIN xnat_experimentData expt ON isd.id=expt.id LEFT JOIN xnat_experimentData_meta_data meta ON expt.experimentData_info=meta.meta_data_id LEFT JOIN xnat_experimentData_share proj ON expt.id=proj.sharing_share_xnat_experimentda_id WHERE (proj.project=:projectId OR expt.project=:projectId) AND (meta.status='active' OR meta.status='locked');";

	/** The parameterized. */
	private final NamedParameterJdbcTemplate _parameterized=XDAT.getContextService().getBean(NamedParameterJdbcTemplate.class);

	/** The intake project. */
	final private String INTAKE_PROJECT="CCF_MDD_ITK"; 

	/** The processing project. */
	final private String PROCESSING_PROJECT="CCF_MDD_PRC";

	/** The carit task name index. */
	final int CARIT_TASK_NAME_INDEX=40;

	/** The carit run number index. */
	final int CARIT_RUN_NUMBER_INDEX=45;

	/** The facematching task name index. */
	final int FACEMATCHING_TASK_NAME_INDEX=20;

	/** The facematching run number index. */
	final int FACEMATCHING_RUN_NUMBER_INDEX=21;

	/** The Constant FACEMATCHING. */
	private static final String FACEMATCHING = "FACEMATCHING";

	/** The Constant CARIT. */
	private static final String CARIT = "CARIT";

	/** The regex for carit and facematching csv. */
	final String REGEX_FOR_CARIT_AND_FACEMATCHING_CSV="((?i)(^((CARIT_.*)|(.*_FACEMATCHING_.*)))+(.CSV$))";

	/** The carit CSV files. */
	List<File> caritCSVFiles= new ArrayList<File>();

	/** The face mathching CSV files. */
	List<File> faceMathchingCSVFiles= new ArrayList<File>();
	
	/**
	 * Instantiates a new MD D IT K T O PR C linked data migrator.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public MDD_ITK_TO_PRC_LinkedDataMigrator(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}

	/**
	 * Instantiates a new MD D IT K T O PR C linked data migrator.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public MDD_ITK_TO_PRC_LinkedDataMigrator(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	/**
	 * Validate.
	 *
	 * @param itkProjectName the itk project name
	 * @param prcProjectName the prc project name
	 * @return true, if successful
	 */
	public boolean validate(String itkProjectName, String prcProjectName)
	{
		return (itkProjectName!=null && itkProjectName.equals("CCF_MDD_ITK") && prcProjectName!=null && prcProjectName.endsWith("CCF_MDD_PRC"));
	}


	/**
	 * Move custom uploader files at project level.
	 *
	 * @param itkProjectName the itk project name
	 * @param prcProjectName the prc project name
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void moveCustomUploaderFilesAtProjectLevel(String itkProjectName, String prcProjectName) throws UnknownPrimaryProjectException, ServerException, ClientException, IOException
	{
		if(validate(itkProjectName, prcProjectName))
		{
			XnatProjectdata intakeProject=XnatProjectdata.getProjectByIDorAlias(itkProjectName, user, false);
			logger.debug("Fetching all seesion Ids associated with Intake project :: "+intakeProject.getName());
			List<String> sessionIds=getAllSessionIds(intakeProject.getId());

			logger.debug("fetched all session Ids. Size :: "+sessionIds!=null?sessionIds.size():0);
			logger.debug("Iterating sessions.");
			for (Iterator<String> iterator = sessionIds.iterator(); iterator.hasNext();) {
				String sessionId =  iterator.next();
				moveCustomUploaderFiles(sessionId, intakeProject);

			}
		}
	}

	/**
	 * Move custom uploader files at project level.
	 *
	 * @param experimentId the experiment id
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void moveCustomUploaderFilesAtSessionLevel(String experimentId) throws UnknownPrimaryProjectException, ServerException, ClientException, IOException
	{
		XnatProjectdata intakeProject=XnatProjectdata.getProjectByIDorAlias(INTAKE_PROJECT, user, false);
		moveCustomUploaderFiles(experimentId,intakeProject);
	}


	/**
	 * Move custom uploader files.
	 *
	 * @param sessionId the session id
	 * @param intakeProject the intake project
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void moveCustomUploaderFiles(String sessionId, XnatProjectdata intakeProject) throws UnknownPrimaryProjectException, ServerException, ClientException, IOException
	{

		logger.debug("Fetching Session :: "+sessionId);
		XnatImagesessiondata imageSessionData= (XnatImagesessiondata) XnatImagesessiondata.getXnatExperimentdatasById(sessionId, user, false);
		logger.debug("Fetching destination session for label :: "+imageSessionData.getLabel());
		exp=getDestinationSession(imageSessionData.getLabel());
		if(exp==null)
		{
			logger.debug("Session "+imageSessionData.getLabel()+" does not exists in Processing Project.");
			logger.debug("*****************************************************************************");
		}
		else 
		{
			logger.debug("Destination session label and ID :: "+exp.getLabel()+"/"+exp.getId());
			proj=exp.getProjectData();
			logger.debug("Creating linked data path for session.");

			//Session level files.
			String linkedDataPath=imageSessionData.getRelativeArchivePath()+"/RESOURCES/LINKED_DATA";


			File linkedDataDir= new File(linkedDataPath);
			if(linkedDataDir.exists() && linkedDataDir.isDirectory() && linkedDataDir.list().length > 0)
			{
				File psychopyOrEVDir=linkedDataDir.listFiles(new DirectoryFilter())[0];
				File tempDir= new File(System.getProperty(JAVA_IO_TMPDIR)+"/"+sessionId);
				if(tempDir.exists()) {
					FileSystemUtils.deleteRecursively(tempDir);
				}
				logger.debug("Session temp dir:: "+tempDir.getAbsolutePath());
				FileUtils.copyDirectory(psychopyOrEVDir, tempDir);
				File[] logFiles=tempDir.listFiles();
				for (int i = 0; i < logFiles.length; i++) {
					if(!logFiles[i].getName().toUpperCase().endsWith("_CATALOG.XML"))
						uploadFileMap.put(logFiles[i], new LinkedFileInfo(exp, psychopyOrEVDir.getName(),false));
				}
				generateTaskRelatedFiles(tempDir); 
			}
			else {
				logger.debug("No Session level files. Directory does not exist:: "+linkedDataPath);
			}

			//Scan level files.
			logger.debug("Checking scans for Custom uploader files");
			List<XnatImagescandataI> scans=imageSessionData.getScans_scan();
			for (Iterator<XnatImagescandataI> scanIterator = scans.iterator(); scanIterator.hasNext();) {
				XnatImagescandataI scan = (AutoXnatImagescandata) scanIterator.next();
				List<XnatResourcecatalog> files=scan.getFile();
				for (Iterator<XnatResourcecatalog> fileIterator = files.iterator(); fileIterator.hasNext();) {
					XnatResourcecatalog file = fileIterator.next();
					if("LINKED_DATA".equals(file.getLabel()))
					{
						logger.debug("Scan #"+scan.getId()+" has Linked_Data directory.");
						XnatImagescandataI destScan=getDestinationScan(scan.getId());
						XnatAbstractresource res = XnatAbstractresource.getXnatAbstractresourcesByXnatAbstractresourceId(file.getXnatAbstractresourceId(), null, false);
						String path=res.getFullPath(intakeProject.getRootArchivePath());
						String tempPath=path.substring(0,path.lastIndexOf("/"));
						File scanLinkedDataDir= new File(tempPath.substring(0,tempPath.lastIndexOf("/")));
						if(scanLinkedDataDir.exists() && scanLinkedDataDir.list().length > 0)
						{
							logger.debug("Linked Data directory exists for scan #"+scan.getId() +" and it has "+res.getFileCount()+" files.");
							File scanPsychopyOrEVDir=scanLinkedDataDir.listFiles(new DirectoryFilter())[0];
							File scanTempDir= new File(System.getProperty(JAVA_IO_TMPDIR)+"/"+sessionId+"_"+scan.getId());
							if(scanTempDir.exists()) {
								FileSystemUtils.deleteRecursively(scanTempDir);
							}
							FileUtils.copyDirectory(scanLinkedDataDir, scanTempDir);
							logger.debug("Scan temp dir:: "+scanTempDir.getAbsolutePath());
							List<File> scanLogFiles=(List<File>) FileUtils.listFiles(scanTempDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
							for (int i = 0; i < scanLogFiles.size(); i++) {
								if(!scanLogFiles.get(i).getName().toUpperCase().endsWith("_CATALOG.XML"))
								{
									uploadFileMap.put(scanLogFiles.get(i), new LinkedFileInfo(destScan,  scanPsychopyOrEVDir.getName()));
								}
							}
							File sessionLevelTaskFile =getFileGeneratedAtSessionLevel(destScan);
							if(sessionLevelTaskFile!=null)
							{
								uploadFileMap.put(sessionLevelTaskFile, new LinkedFileInfo(destScan,  scanPsychopyOrEVDir.getName()));
							}
						}
						logger.info(imageSessionData.getLabel()+"-->"+scan.getId()+"-->"+scan.getType()+"-->"+file.getLabel()+"-->"+file.getFileCount());
					}
				}
			}
			Boolean upload_status=uploadFilesToResources(uploadFileMap);
			uploadFileMap.clear();
			cleanup(sessionId);
			logger.info("Upload Status for session "+imageSessionData.getLabel()+" :: "+upload_status);
			logger.debug("*****************************************************************************");
		}
	}

	/**
	 * Gets the file generated at session level.
	 *
	 * @param destScan the dest scan
	 * @return the file generated at session level
	 */
	private File getFileGeneratedAtSessionLevel(XnatImagescandataI destScan) {
		File f=null;
		if(destScan.getSeriesDescription().toUpperCase().contains(CARIT))
			f=caritCSVFiles.remove(0);
		else if(destScan.getSeriesDescription().toUpperCase().contains(FACEMATCHING))
			f=faceMathchingCSVFiles.remove(0);
		return f;
	}

	/**
	 * Generate task related files.
	 *
	 * @param linkedDataDir the linked data dir
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void generateTaskRelatedFiles(File linkedDataDir) throws IOException {

		FileFilter fileFilter = new RegexFileFilter(Pattern.compile(REGEX_FOR_CARIT_AND_FACEMATCHING_CSV));
		File[] files=linkedDataDir.listFiles(fileFilter); 
		if(files.length>0)
			for(int i=0;i<files.length;i++)
			{
				if(!files[i].getName().toUpperCase().contains("DESIGN"))
				{
					Reader reader = Files.newBufferedReader(Paths.get(files[i].getPath()),StandardCharsets.UTF_8);
					CSVReader csvReader = new CSVReader(reader);

					String[] rowData;
					String oldKey=null;
					CSVWriter csvWriter= null;
					String[] header =csvReader.readNext();
					while((rowData= csvReader.readNext()) != null)
					{
						String key=null;
						if(files[i].getName().toUpperCase().contains(CARIT))
							key=rowData[CARIT_TASK_NAME_INDEX]+"_"+rowData[CARIT_RUN_NUMBER_INDEX];
						else if(files[i].getName().toUpperCase().contains(FACEMATCHING))
							key=rowData[FACEMATCHING_TASK_NAME_INDEX]+"_"+rowData[FACEMATCHING_RUN_NUMBER_INDEX];
						else
						{
							logger.error("Invalid file Name :"+ files[i].getPath());
							continue;
						}

						if(oldKey==null || csvWriter == null)
						{
							oldKey=key;
							csvWriter= new CSVWriter(Files.newBufferedWriter(Paths.get(linkedDataDir.getPath()+File.separator+key+".csv"),StandardCharsets.UTF_8));
							if(key.toUpperCase().startsWith(CARIT))
								caritCSVFiles.add(new File(linkedDataDir.getPath()+File.separator+key+".csv"));
							if(key.toUpperCase().startsWith(FACEMATCHING))
								faceMathchingCSVFiles.add(new File(linkedDataDir.getPath()+File.separator+key+".csv"));
							csvWriter.writeNext(header);

						}
						if(oldKey.equals(key))
						{
							csvWriter.writeNext(rowData);
						}
						else
						{		
							closeWriter(csvWriter);
							csvWriter = null;
							oldKey=key;
						}

					}
					if(csvWriter!=null)
					{
						closeWriter(csvWriter);
					}
					csvReader.close();
				}
			}
		else
		{
			logger.error("csv files  does not exists for "+linkedDataDir.getPath());
		}
	}

	/**
	 * Close writer.
	 *
	 * @param csvWriter the csv writer
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void closeWriter(CSVWriter csvWriter) throws IOException {
		csvWriter.flush();
		csvWriter.close();
	}

	/**
	 * Cleanup.
	 *
	 * @param sessionId the session id
	 */
	private void cleanup(final String sessionId) {
		File tempfile=new File(System.getProperty(JAVA_IO_TMPDIR));
		File[] files=tempfile.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(sessionId);
			}
		});

		for (File file2 : files) {
			try {
				logger.debug("Deleted directory :: "+file2.getAbsolutePath());
				FileUtils.deleteDirectory(file2);
			} catch (IOException e) {
				logger.debug("Unable to delete directory :: "+file2.getAbsolutePath());
				e.printStackTrace();
			}
		}


	}

	/**
	 * Gets the destination session.
	 *
	 * @param sessionId the session id
	 * @return the destination session
	 */
	private XnatImagesessiondata getDestinationSession(String sessionId)
	{
		CriteriaCollection cc= new CriteriaCollection("AND");
		cc.addClause("xnat:imageSessionData/label", sessionId);
		cc.addClause("xnat:imageSessionData/project", PROCESSING_PROJECT);
		ArrayList<XnatImagesessiondata> session=XnatImagesessiondata.getXnatImagesessiondatasByField(cc, user, false);
		return session.size()>0?session.get(0):null;
	}

	/**
	 * Gets the destination scan.
	 *
	 * @param scanId the scan id
	 * @return the destination scan
	 */
	private XnatImagescandata getDestinationScan(String scanId)
	{
		CriteriaCollection cc= new CriteriaCollection("AND");
		cc.addClause("xnat:imageScanData/ID", scanId);
		cc.addClause("xnat:imageScanData/image_session_ID", exp.getId());
		cc.addClause("xnat:imageScanData/project", PROCESSING_PROJECT);
		ArrayList<XnatImagescandata> scans=XnatImagescandata.getXnatImagescandatasByField(cc, user, false);
		return scans.size()>0?scans.get(0):null;
	}

	/**
	 * Gets the all session ids.
	 *
	 * @param projectId the project id
	 * @return the all session ids
	 */
	private List<String> getAllSessionIds(String projectId)
	{
		final Map<String, String> attributes = Maps.newHashMap();
		attributes.put("projectId", projectId);
		final List<String> sessionIds = _parameterized.queryForList(QUERY_IMG_SESSION_IDS, attributes, String.class);
		return sessionIds;
	}

	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		try {
			//moveCustomUploaderFiles(INTAKE_PROJECT, PROCESSING_PROJECT);
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		} 		
	}

	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#getScanModifier(org.nrg.xdat.model.XnatImagescandataI, org.nrg.xft.event.EventMetaI)
	 */
	@Override
	protected DirectScanResourceImpl getScanModifier(final XnatImagescandataI scan,final EventMetaI ci) {
		final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl((XnatImagescandata)scan,exp,true,user,ci);
		return scanModifier;
	}
}
