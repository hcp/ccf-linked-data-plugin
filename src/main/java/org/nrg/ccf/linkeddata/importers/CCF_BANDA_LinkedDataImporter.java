/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FileFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;

/**
 * The Class CCF_BANDA_LinkedDataImporter.
 *
 * @author Atul
 */
public class CCF_BANDA_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_BANDA_LinkedDataImporter.class);

	/** The Constant TASK_NAME_GAMBLING. */
	private static final String TASK_NAME_GAMBLING = "Gambling";

	/** The Constant TASK_NAME_FACEMATCHING. */
	private static final String TASK_NAME_FACEMATCHING = "FaceMatching";

	/** The Constant TASK_NAME_RESTING. */
	private static final String TASK_NAME_RESTING = "Resting";

	/** The Constant TASK_NAME_CONFLICT. */
	private static final String TASK_NAME_CONFLICT = "conflict";

	/** The gambling list. */
	private List<XnatImagescandataI> gamblingList=new ArrayList<XnatImagescandataI>();

	/** The face matching list. */
	private List<XnatImagescandataI> faceMatchingList=new ArrayList<XnatImagescandataI>();

	/** The Resting list. */
	private List<XnatImagescandataI> restingList=new ArrayList<XnatImagescandataI>();

	/** The conflict list. */
	private List<XnatImagescandataI> conflictList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CCF BANDA linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_BANDA_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CCF BANDA linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_BANDA_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.
	 * File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(cacheLoc.getAbsolutePath());
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);
		
		// Start matching files to session
		returnList.add("<br><b>PROCESS SESSION-LEVEL FILES</b>");
		processSessionLevelFiles(scanLevelDir, exp.getSubjectData().getLabel());
		
		Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);
		
		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}

	/**
	 * 
	 */
	private void processSessionLevelFiles(File scanLevelDir,final String subjectName) {
		File[] sessionLevelFile=scanLevelDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.getName().toUpperCase().equals((subjectName+".TXT").toUpperCase()) ;
			}
		});
		if(sessionLevelFile!=null && sessionLevelFile.length!=0) {
			uploadFileMap.put(sessionLevelFile[0], new LinkedFileInfo(null, Constants.SESSION_PSYCHOPY_FOLDER));
			leftOverFiles.remove(sessionLevelFile[0]);
		}
			
		
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split("_");
			if(sdParts!=null && sdParts.length==5)
			{
				if(TASK_NAME_GAMBLING.equalsIgnoreCase(sdParts[3]))
				{
					gamblingList.add(scan);
				}
				else if(TASK_NAME_FACEMATCHING.equalsIgnoreCase(sdParts[3]))
				{
					faceMatchingList.add(scan);
				}
				else if(TASK_NAME_RESTING.equalsIgnoreCase(sdParts[3])||"REST".equalsIgnoreCase(sdParts[3]))
				{
					restingList.add(scan);
				}
				else if(TASK_NAME_CONFLICT.equalsIgnoreCase(sdParts[3]))
				{
					conflictList.add(scan);
				}
			}
		}
		LinkedDataUtil.sortTheScanList(gamblingList, getReturnList());
		LinkedDataUtil.sortTheScanList(faceMatchingList, getReturnList());
		LinkedDataUtil.sortTheScanList(restingList, getReturnList());
		LinkedDataUtil.sortTheScanList(conflictList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param scanLevelDir the scan level dir
	 */
	private void processScanLevelTaskDataFiles(File scanLevelDir) {
		try {
			File[] taskFiles=scanLevelDir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return !file.getName().toUpperCase().startsWith("TEMP_") && !file.getName().toUpperCase().contains("_PRACTICE_");
				}
			});

			Map<String,ArrayList<File>> fileMap=new HashMap<String,ArrayList<File>>();
			for (int i = 0; i < taskFiles.length; i++) 
			{
				if(taskFiles[i].isFile())
				{
					String fileName=taskFiles[i].getName().substring(0, taskFiles[i].getName().lastIndexOf("."));
					if(fileMap.get(fileName)==null) {
						fileMap.put(fileName,new ArrayList<File>());
					}
					if(taskFiles[i].getName().startsWith(fileName))
					{
						fileMap.get(fileName).add(taskFiles[i]);
					}
				}
			}

			for (Iterator<String> iterator = fileMap.keySet().iterator(); iterator.hasNext();) {
				String fileName=iterator.next();

				XnatImagescandataI scan=null;
				if(fileName.contains(TASK_NAME_GAMBLING))
				{
					scan=gamblingList.remove(0);
				}
				else if(fileName.contains(TASK_NAME_FACEMATCHING))
				{
					scan=faceMatchingList.remove(0);
				}
				else if(fileName.contains(TASK_NAME_RESTING))
				{
					scan=restingList.remove(0);
				}
				else if(fileName.contains(TASK_NAME_CONFLICT))
				{
					scan=conflictList.remove(0);
				}
				
				if(scan!=null)
				{
					ArrayList<File> files=fileMap.get(fileName);
					for (int j = 0; j < files.size(); j++) {
						uploadFileMap.put(files.get(j), new LinkedFileInfo(scan, Constants.SCAN_PSYCHOPY_FOLDER));
					}
					files.clear();
				}

			}
			for (Iterator<String> iterator = fileMap.keySet().iterator(); iterator.hasNext();) {
				leftOverFiles.addAll(fileMap.get(iterator.next()));
			}
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
			throw e;
		}
	}

}
