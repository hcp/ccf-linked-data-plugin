/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.DirectoryFilter;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.FileSystemUtils;

import com.google.common.collect.Maps;

/**
 * The Class ITK_TO_PRC_LinkedDataImporter will migrate all linked data from
 * Intake to Processing projects.
 * 
 * @author Atul
 */
public class ITK_TO_PRC_LinkedDataImporter extends AbstractLinkedDataImporter {

	/**
	 * 
	 */
	private static final String RESOURCES_DIRECTORY_NAME = "RESOURCES";

	/**
	 * 
	 */
	private static final String LINKED_DATA_DIRECTORY_NAME = "LINKED_DATA";

	/**
	 * 
	 */
	private static final String _CATALOG_XML_FILE = "_CATALOG.XML";

	/** The Constant LINKED_DATA_DIRECTORY. */
	private static final String LINKED_DATA_DIRECTORY = LINKED_DATA_DIRECTORY_NAME;

	/** The logger. */
	static Logger logger = Logger.getLogger(ITK_TO_PRC_LinkedDataImporter.class);

	/** The Constant JAVA_IO_TMPDIR. */
	private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";

	/** The upload file map. */
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The Constant QUERY_IMG_SESSION_IDS. */
	private static final String QUERY_IMG_SESSION_IDS = "SELECT DISTINCT isd.id FROM xnat_imageSessionData isd LEFT JOIN xnat_experimentData expt ON isd.id=expt.id LEFT JOIN xnat_experimentData_meta_data meta ON expt.experimentData_info=meta.meta_data_id LEFT JOIN xnat_experimentData_share proj ON expt.id=proj.sharing_share_xnat_experimentda_id WHERE (proj.project=:projectId OR expt.project=:projectId) AND (meta.status='active' OR meta.status='locked');";

	/** The parameterized. */
	private final NamedParameterJdbcTemplate _parameterized = XDAT.getContextService()
			.getBean(NamedParameterJdbcTemplate.class);

	/**
	 * Instantiates a new ITK TO PRC linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u               the u
	 * @param fw              the fw
	 * @param params          the params
	 */
	public ITK_TO_PRC_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}

	/**
	 * Instantiates a new IT K T O PR C linked data importer.
	 *
	 * @param u      the u
	 * @param params the params
	 */
	public ITK_TO_PRC_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	/**
	 * Validate the project details. Code will also check if the investigators in
	 * ITK and PRC projects are same.
	 *
	 * @param itkProjectName the itk project name
	 * @param prcProjectName the prc project name
	 * @return true, if successful
	 */
	public boolean validate(String itkProjectName, String prcProjectName) {
		XnatProjectdata intakeProject = XnatProjectdata.getProjectByIDorAlias(itkProjectName, user, false);
		XnatProjectdata prcProject = XnatProjectdata.getProjectByIDorAlias(prcProjectName, user, false);
		return (itkProjectName != null && itkProjectName.endsWith("_ITK") && prcProjectName != null
				&& prcProjectName.endsWith("_PRC") && prcProject != null
				&& intakeProject.getInvestigators_investigator().equals(prcProject.getInvestigators_investigator()));
	}

	/**
	 * Move custom uploader files at project level.
	 *
	 * @param itkProjectName the itk project name
	 * @param prcProjectName the prc project name
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException                the server exception
	 * @throws ClientException                the client exception
	 * @throws IOException                    Signals that an I/O exception has
	 *                                        occurred.
	 */
	public void moveCustomUploaderFilesAtProjectLevel(String itkProjectName, String prcProjectName)
			throws UnknownPrimaryProjectException, ServerException, ClientException, IOException {
		Set<String> failedData = new HashSet<String>();
		try {
			logger.debug("Initiating Project level Linked data migration. Intake Project ="+itkProjectName +", Processing Project = "+prcProjectName);
			if (validate(itkProjectName, prcProjectName)) {
				XnatProjectdata intakeProject = XnatProjectdata.getProjectByIDorAlias(itkProjectName, user, false);
				proj =XnatProjectdata.getProjectByIDorAlias(prcProjectName, user, false);
				logger.debug("Fetching all seesion Ids associated with Intake project :: " + intakeProject.getName());
				List<String> sessionIds = getAllSessionIds(intakeProject.getId());

				logger.debug("fetched all session Ids. Size :: " + sessionIds != null ? sessionIds.size() : 0);
				logger.debug("Iterating sessions.");
				for (Iterator<String> iterator = sessionIds.iterator(); iterator.hasNext();) {
					String sessionId = iterator.next();
					logger.debug("Move files for sessionId:: "+sessionId);
					try {
						moveCustomUploaderFiles(sessionId, intakeProject, prcProjectName);
					}catch(Exception e)
					{
						failedData.add(sessionId);
						logger.error(ExceptionUtils.getStackTrace(e));
					}
				}
			}
		}catch(Exception e)
		{
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e; 
		}
		finally {
			if(!failedData.isEmpty())
			{
				StringBuilder message= new StringBuilder("\nThe Project level linked data migration process has failed for the following experiments: \n");
				for (Iterator<String> iterator = failedData.iterator(); iterator.hasNext();) {
					message.append( iterator.next());
				}
				logger.error(message.toString());
			}
		}
	}

	/**
	 * Move custom uploader files at session level.
	 *
	 * @param intakeProjectName the intake project name
	 * @param prcProjectName    the prc project name
	 * @param experimentId      the experiment id
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException                the server exception
	 * @throws ClientException                the client exception
	 * @throws IOException                    Signals that an I/O exception has
	 *                                        occurred.
	 */
	public void moveCustomUploaderFilesAtSessionLevel(String intakeProjectName, String prcProjectName,
			String experimentId) throws UnknownPrimaryProjectException, ServerException, ClientException, IOException {
		try {
			XnatProjectdata intakeProject = XnatProjectdata.getProjectByIDorAlias(intakeProjectName, user, false);
			proj =XnatProjectdata.getProjectByIDorAlias(prcProjectName, user, false);
			moveCustomUploaderFiles(experimentId, intakeProject, prcProjectName);
		}catch(Exception e)
		{
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e; 
		}
	}

	/**
	 * Move custom uploader files.
	 *
	 * @param sessionId      the session id
	 * @param intakeProject  the intake project
	 * @param prcProjectName the prc project name
	 * @throws UnknownPrimaryProjectException the unknown primary project exception
	 * @throws ServerException                the server exception
	 * @throws ClientException                the client exception
	 * @throws IOException                    Signals that an I/O exception has
	 *                                        occurred.
	 */
	public void moveCustomUploaderFiles(String sessionId, XnatProjectdata intakeProject, String prcProjectName)
			throws UnknownPrimaryProjectException, ServerException, ClientException, IOException {
		logger.debug("Fetching Session :: " + sessionId);
		XnatImagesessiondata imageSessionData = (XnatImagesessiondata) XnatImagesessiondata
				.getXnatExperimentdatasById(sessionId, user, false);
		logger.debug("Fetching destination session for label :: " + imageSessionData.getLabel());
		logger.debug("Experiment value = "+exp);
		exp = getDestinationSession(imageSessionData.getLabel(), prcProjectName);
		if (exp == null) {
			logger.debug("Session " + imageSessionData.getLabel() + " does not exists in Processing Project.");
			logger.debug("*****************************************************************************");
		} else {
			logger.debug("Destination session label and ID :: " + exp.getLabel() + File.separator + exp.getId());
			//proj = exp.getProjectData();
			logger.debug("Creating linked data path for session.");

			// Session level files.
			String linkedDataPath = imageSessionData.getRelativeArchivePath() + File.separator +RESOURCES_DIRECTORY_NAME+  File.separator +LINKED_DATA_DIRECTORY_NAME;

			File linkedDataDir = new File(linkedDataPath);
			if (linkedDataDir.exists() && linkedDataDir.isDirectory() && linkedDataDir.list().length > 0) {
				if(linkedDataDir.listFiles(new DirectoryFilter())!=null && linkedDataDir.listFiles(new DirectoryFilter()).length >0)
				{
					File psychopyOrEVDir = linkedDataDir.listFiles(new DirectoryFilter())[0];
					File tempDir = new File(System.getProperty(JAVA_IO_TMPDIR) + File.separator + sessionId);
					if (tempDir.exists()) {
						FileSystemUtils.deleteRecursively(tempDir);
					}
					logger.debug("Session temp dir:: " + tempDir.getAbsolutePath());
					FileUtils.copyDirectory(psychopyOrEVDir, tempDir);
					File[] logFiles = tempDir.listFiles();
					for (int i = 0; i < logFiles.length; i++) {
						if (!logFiles[i].getName().toUpperCase().endsWith(_CATALOG_XML_FILE))
							uploadFileMap.put(logFiles[i], new LinkedFileInfo(exp, psychopyOrEVDir.getName(), false));
					}
				}
			} else {
				logger.debug("No Session level files. Directory does not exist:: " + linkedDataPath);
			}

			// Scan level files.
			logger.debug("Checking scans for Custom uploader files");
			List<XnatImagescandataI> scans = imageSessionData.getScans_scan();
			for (Iterator<XnatImagescandataI> scanIterator = scans.iterator(); scanIterator.hasNext();) {
				XnatImagescandataI scan = (AutoXnatImagescandata) scanIterator.next();
				List<XnatResourcecatalog> files = scan.getFile();
				for (Iterator<XnatResourcecatalog> fileIterator = files.iterator(); fileIterator.hasNext();) {
					XnatResourcecatalog file = fileIterator.next();
					if (LINKED_DATA_DIRECTORY.equals(file.getLabel())) {
						logger.debug("Scan #" + scan.getId() + " has Linked_Data directory.");
						XnatImagescandataI destScan = getDestinationScan(scan.getId(), prcProjectName);
						if(destScan == null) {
							logger.debug("Destination scan is missing. Scan id ::"+scan.getId());
						}
						else {
							logger.debug("Destination Scan #" + destScan.getId()   +" , Scan Type: "+destScan.getType());
							XnatAbstractresource res = XnatAbstractresource
									.getXnatAbstractresourcesByXnatAbstractresourceId(file.getXnatAbstractresourceId(),
											null, false);
							String path = res.getFullPath(intakeProject.getRootArchivePath());
							String tempPath = path.substring(0, path.lastIndexOf(File.separator));
							File scanLinkedDataDir = new File(tempPath.substring(0, tempPath.lastIndexOf(File.separator)));
							if (scanLinkedDataDir.exists() && scanLinkedDataDir.list().length > 0) {
								logger.debug("Linked Data directory exists for scan #" + scan.getId() + " and it has "
										+ res.getFileCount() + " files.");
								if(scanLinkedDataDir.listFiles(new DirectoryFilter())!=null && scanLinkedDataDir.listFiles(new DirectoryFilter()).length >0)
								{
									File scanPsychopyOrEVDir = scanLinkedDataDir.listFiles(new DirectoryFilter())[0];
									File scanTempDir = new File(
											System.getProperty(JAVA_IO_TMPDIR) + File.separator + sessionId + "_" + scan.getId());
									if (scanTempDir.exists()) {
										FileSystemUtils.deleteRecursively(scanTempDir);
									}
									FileUtils.copyDirectory(scanLinkedDataDir, scanTempDir);
									logger.debug("Scan temp dir:: " + scanTempDir.getAbsolutePath());
									List<File> scanLogFiles = (List<File>) FileUtils.listFiles(scanTempDir,
											TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
									for (int i = 0; i < scanLogFiles.size(); i++) {
										if (!scanLogFiles.get(i).getName().toUpperCase().endsWith(_CATALOG_XML_FILE)) {
											uploadFileMap.put(scanLogFiles.get(i),
													new LinkedFileInfo(destScan, scanPsychopyOrEVDir.getName()));
											logger.debug("uploading "+scanLogFiles.get(i) +" to "+destScan.getId());
										}
									}
								}
							}
							logger.info(imageSessionData.getId()+ "-->" +imageSessionData.getLabel() + "-->" + scan.getId() + "-->" + scan.getType() + "-->"
									+ file.getLabel() + "-->" + file.getFileCount());
						}
					}
				}
			}
			Boolean upload_status = uploadFilesToResources(uploadFileMap);
			uploadFileMap.clear();
			cleanup(sessionId);
			exp=null;
			logger.info("Upload Status for session " + imageSessionData.getLabel() + " :: " + upload_status);
			logger.debug("*****************************************************************************");
		}
	}

	/**
	 * Cleanup.
	 *
	 * @param sessionId the session id
	 */
	private void cleanup(final String sessionId) {
		File tempfile = new File(System.getProperty(JAVA_IO_TMPDIR));
		File[] files = tempfile.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(sessionId);
			}
		});

		for (File file2 : files) {
			try {
				logger.debug("Deleted directory :: " + file2.getAbsolutePath());
				FileUtils.deleteDirectory(file2);
			} catch (IOException e) {
				logger.debug("Unable to delete directory :: " + file2.getAbsolutePath());
				logger.error(ExceptionUtils.getStackTrace(e));
			}
		}

	}

	/**
	 * Gets the destination session.
	 *
	 * @param sessionId      the session id
	 * @param prcProjectName the prc project name
	 * @return the destination session
	 */
	private XnatImagesessiondata getDestinationSession(String sessionId, String prcProjectName) {
		CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause("xnat:imageSessionData/label", sessionId);
		cc.addClause("xnat:imageSessionData/project", prcProjectName);
		ArrayList<XnatImagesessiondata> session = XnatImagesessiondata.getXnatImagesessiondatasByField(cc, user, false);
		return session.size() > 0 ? session.get(0) : null;
	}

	/**
	 * Gets the destination scan.
	 *
	 * @param scanId         the scan id
	 * @param prcProjectName the prc project name
	 * @return the destination scan
	 */
	private XnatImagescandata getDestinationScan(String scanId, String prcProjectName) {
		CriteriaCollection cc = new CriteriaCollection("AND");
		cc.addClause("xnat:imageScanData/ID", scanId);
		cc.addClause("xnat:imageScanData/image_session_ID", exp.getId());
		cc.addClause("xnat:imageScanData/project", prcProjectName);
		ArrayList<XnatImagescandata> scans = XnatImagescandata.getXnatImagescandatasByField(cc, user, false);
		return scans.size() > 0 ? scans.get(0) : null;
	}

	/**
	 * Gets the all session ids.
	 *
	 * @param projectId the project id
	 * @return the all session ids
	 */
	private List<String> getAllSessionIds(String projectId) {
		final Map<String, String> attributes = Maps.newHashMap();
		attributes.put("projectId", projectId);
		final List<String> sessionIds = _parameterized.queryForList(QUERY_IMG_SESSION_IDS, attributes, String.class);
		return sessionIds;
	}

	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		// TODO Auto-generated method stub

	}
}
