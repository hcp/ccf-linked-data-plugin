/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FileFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;

/**
 * @author Atul
 *
 */
public class CCF_MCW_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_MCW_LinkedDataImporter.class);

	List<XnatImagesessiondata> expList= new ArrayList<XnatImagesessiondata>();

	/** The upload file map. */
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();
	
	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();
	/**
	 * @param u
	 * @param params
	 */
	public CCF_MCW_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
		// TODO Auto-generated constructor stub
	}

	public CCF_MCW_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, subject.getLabel()));

		returnList.add(Constants.PROCESS_SESSION_LEVEL_FILES_MESSAGE);

		File psychopyDir=new File(new StringBuilder(cacheLoc.getAbsolutePath()).toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(psychopyDir, null, true);
		Boolean upload_status=false;

		if(psychopyDir.exists() && psychopyDir.isDirectory())
			processSessionLevelFiles(psychopyDir);
		else
			returnList.add(Constants.PSYCHOPY_DIRECTORY_DOES_NOT_EXISTS);

		//Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));

	}

	/**
	 * Process session level files.
	 *
	 * @param psychopyDir the psychopy dir
	 * @return true, if successful
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	private boolean processSessionLevelFiles(File psychopyDir) throws ClientException, ServerException {
		try
		{
			for (Iterator<XnatImagesessiondata> iterator = expList.iterator(); iterator.hasNext();) {
				final XnatImagesessiondata xnatImagesessiondata =  iterator.next();

				File[] psychopyfiles=psychopyDir.listFiles(new FileFilter() {
					
					@Override
					public boolean accept(File file) {
						return file.isFile() && file.getName().substring(file.getName().indexOf("_")+1).toUpperCase().startsWith(xnatImagesessiondata.getLabel().toUpperCase());
					}
				});
				for (int i = 0; i < psychopyfiles.length; i++) {
					exp=xnatImagesessiondata;
					uploadFileMap.put(psychopyfiles[i], new LinkedFileInfo(null,  Constants.SESSION_PSYCHOPY_FOLDER));
				}
				if(psychopyfiles.length>0)
					uploadFilesToResources(uploadFileMap);
			}
			
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public List<String> callFromAutomationUploader() throws ClientException, ServerException {
		if (!this.params.containsKey("BuildPath")) {
			throw new ClientException("ERROR:  BuildPath was not specified");
		}
		verifyAndGetSubject();
		try {
			processCacheFiles(new File(this.params.get("BuildPath").toString()));
			return this.getReturnList();
		} catch (ClientException e) {
			logger.error("",e);
			this.getReturnList().add("<br>" + e.toString());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			this.getReturnList().add("<br>" + e.toString());
			throw e;
		} catch (Throwable e) {
			logger.error(ExceptionUtils.getStackTrace(e));
			this.getReturnList().add("<br>" + e.toString());
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	/**
	 * @throws ClientException 
	 * 
	 */
	private void verifyAndGetSubject() throws ClientException {
		String projID=null;
		if (params.get("project")!=null) {
			projID=params.get("project").toString();
		}
		String subjLbl=null;
		if (params.get("subject")==null) {
			//subjLbl=params.get("subject").toString();
			clientFailed("ERROR:  subject parameter (containing subject label) must be supplied for import");
		}
		String subjectLbl=params.get("subject").toString();
		// NOTE:  This call failed when calling from a script when setting the preload parameter to false.  It worked okay from inside the session.
		this.subject =XnatSubjectdata.getXnatSubjectdatasById(subjectLbl, user, true);
		if(subject!=null){
			// NOTE:  Also setting preload parameter to true here.  Likely otherwise wouldn't work from scripts.
			
			List<XnatSubjectassessordata> experiments=subject.getExperiments_experiment();
			for (Iterator<XnatSubjectassessordata> iterator = experiments.iterator(); iterator.hasNext();) {
				XnatSubjectassessordata xnatSubAssessordata =  iterator.next();
				
				if (projID!=null && !xnatSubAssessordata.getProject().equalsIgnoreCase(projID)) {
					iterator.remove();
					continue;
				}
				if (subjLbl!=null && 
						!(xnatSubAssessordata.getSubjectData().getId().equalsIgnoreCase(subjLbl) || xnatSubAssessordata.getSubjectData().getLabel().equalsIgnoreCase(subjLbl))) {
					iterator.remove();
					continue;
				}
				expList.add((XnatImagesessiondata)xnatSubAssessordata);
			}
		}
		if (expList==null || expList.isEmpty()) {
			clientFailed("ERROR:  Could not find matching experiment for import");
		}
		proj=subject.getProjectData();

	}
}
