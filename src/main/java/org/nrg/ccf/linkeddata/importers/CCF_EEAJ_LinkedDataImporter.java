/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FilenameFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;



/**
 * The Class CCF_EEAJ_LinkedDataImporter.
 * @author Atul
 */
public class CCF_EEAJ_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_EEAJ_LinkedDataImporter.class);

	/** The Constant TASK_NAME_MOTOR. */
	private static final String TASK_NAME_MOTOR = "MOTOR";

	/** The Constant TASK_NAME_LANGUAGE. */
	private static final String TASK_NAME_LANGUAGE = "LANGUAGE";
	
	/** The Constant TASK_NAME_STORYMATH. */
	private static final String TASK_NAME_STORYMATH = "STORYMATH";

	/** The motor list. */
	private List<XnatImagescandataI> motorList=new ArrayList<XnatImagescandataI>();

	/** The language list. */
	private List<XnatImagescandataI> languageList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CCF EEAJ linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_EEAJ_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CCF EEAJ linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_EEAJ_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(scanLevelDir, null, true);
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		//Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:true, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split("_");
			if(sdParts!=null && sdParts.length>1)
			{
				if(Arrays.asList(sdParts).contains(TASK_NAME_MOTOR) && sdParts.length==4)
				{
					motorList.add(scan);
				}
				else if(Arrays.asList(sdParts).contains(TASK_NAME_LANGUAGE) && sdParts.length==2)
				{
					languageList.add(scan);
				}
			}
		}
		LinkedDataUtil.sortTheScanList(motorList, getReturnList());
		LinkedDataUtil.sortTheScanList(languageList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir.
	 */
	private void processScanLevelTaskDataFiles(File taskDir) {
		try {
			//get motor task files
			associateTaskFiles(taskDir,motorList,TASK_NAME_MOTOR);
			//get Language task files
			associateTaskFiles(taskDir,languageList,TASK_NAME_LANGUAGE);
			uploadFilesToResources(uploadFileMap);
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
		}
	}


	/**
	 * Associate task files.
	 *
	 * @param taskDir the task dir
	 * @param taskList the task list
	 * @param taskName the task name
	 * @return true, if successful
	 * @throws ServerException the server exception
	 * @throws ClientException the client exception
	 */
	private void associateTaskFiles(File taskDir, List<XnatImagescandataI> taskList, final String taskName) throws ServerException, ClientException {
		File[] files=taskDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if(TASK_NAME_MOTOR.equalsIgnoreCase(taskName))
					return name.toUpperCase().contains(taskName) && !name.toUpperCase().contains("RUNP") && !name.toUpperCase().startsWith(".");
				else
					return StringUtils.containsAny(name.toUpperCase(), TASK_NAME_LANGUAGE,TASK_NAME_STORYMATH) && !name.toUpperCase().contains("RUNP") && !name.toUpperCase().startsWith(".");
			}
		});
		for (int i = 0; i < files.length; i++) {
			uploadFileMap.put(files[i], new LinkedFileInfo(taskList.get(0), Constants.SESSION_PSYCHOPY_FOLDER));
		} 
	}
}
