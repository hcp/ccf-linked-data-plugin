package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_ANXPE_LinkedDataImporter.
 * 
 * @author Atul
 */
public class CCF_ANXPE_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_ANXPE_LinkedDataImporter.class);

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CC F ANXP E linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_ANXPE_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CC F ANXP E linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_ANXPE_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}


	/**
	 * Process cache files.
	 *
	 * @param cacheLoc the cache loc
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File sessionLevelDir = new File(cacheLoc.getAbsolutePath());
		processSessionLevelFiles(sessionLevelDir, exp.getSubjectData().getLabel());
		Boolean upload_status=uploadFilesToResources(uploadFileMap);
		LinkedDataUtil.addUploadStatus(upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}

	/**
	 * Process session level files.
	 *
	 * @param scanLevelDir the scan level dir
	 * @param subjectName the subject name
	 */
	private void processSessionLevelFiles(File scanLevelDir,final String subjectName) {
		File[] sessionLevelFile=scanLevelDir.listFiles();
		if(sessionLevelFile!=null && sessionLevelFile.length!=0) {
			for(int i=0;i< sessionLevelFile.length ;i++)
				uploadFileMap.put(sessionLevelFile[i], new LinkedFileInfo(null, Constants.SESSION_PSYCHOPY_FOLDER));
		}
	}
}
