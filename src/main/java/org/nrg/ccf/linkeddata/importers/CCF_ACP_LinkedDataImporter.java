package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_ACP_LinkedDataImporter.
 * @author Atul
 */
public class CCF_ACP_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_ACP_LinkedDataImporter.class);

	/** The Constant TASK_NAME_WORKING_MEMORY. */
	private static final String TASK_NAME_WORKING_MEMORY = "WORKING MEMORY";
	
	/** The Constant TASK_NAME_EMOTION. */
	private static final String TASK_NAME_EMOTION = "EMOTION";
	
	/** The emotion list. */
	private List<XnatImagescandataI> emotionList=new ArrayList<XnatImagescandataI>();
	
	/** The working mem list. */
	private List<XnatImagescandataI> workingMemList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CCF ACP linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_ACP_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CC F AC P linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_ACP_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		System.out.println("hello");
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(scanLevelDir, null, true);
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) 
		{
			final String[] sdParts = scan.getSeriesDescription().split(Constants.UNDERSCORE);
			if(sdParts!=null && sdParts.length>1)
			{
				if(scan.getSeriesDescription().toUpperCase().startsWith(TASK_NAME_EMOTION) && sdParts.length==3)
				{
					emotionList.add(scan);
				}
				else if(scan.getSeriesDescription().toUpperCase().startsWith(TASK_NAME_WORKING_MEMORY) && sdParts.length==3)
				{
					workingMemList.add(scan);
				}
			}
		}
		LinkedDataUtil.sortTheScanList(emotionList, getReturnList());
		LinkedDataUtil.sortTheScanList(workingMemList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir
	 */
	private void processScanLevelTaskDataFiles(File taskDir) {
		try {
			File[] taskFiles=taskDir.listFiles();
			Arrays.sort(taskFiles);
			List<File> fileList= Arrays.asList(taskFiles);
			List<File> filesToRemove = new ArrayList<File>();
			addFilesToScan(fileList,emotionList,filesToRemove,"_EMOTION_");
			addFilesToScan(fileList,workingMemList,filesToRemove,"_WM_");
			
			/*for (int i = 0; i < taskFiles.length; i++) {
				List<XnatImagescandataI> scanlist=getTaskList(taskFiles[i].getName());
				if(scanlist!=null && !scanlist.isEmpty())
				{
					XnatImagescandata scan = (XnatImagescandata) scanlist.remove(0);
					uploadFileMap.put(taskFiles[i], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
				}
			}*/
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
		}
	}


	private void addFilesToScan(List<File> taskFiles, List<XnatImagescandataI> scanList, List<File> filesToRemove, String scanIdentifierString) {
		Iterator<XnatImagescandataI> emotIter=scanList.iterator();
		while(emotIter.hasNext())
		{
			String previousFileName=null;
			XnatImagescandata scan = (XnatImagescandata) emotIter.next();
			Iterator<File> taskFilesIter=taskFiles.iterator();
			while(taskFilesIter.hasNext())
			{
				File taskFile=taskFilesIter.next();
				if(taskFile.getName().toUpperCase().contains(scanIdentifierString) && !filesToRemove.contains(taskFile)) 
				{
					if(previousFileName == null )
					{
						previousFileName=taskFile.getName().substring(0, taskFile.getName().lastIndexOf("."));
						uploadFileMap.put(taskFile, new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
						filesToRemove.add(taskFile);
					}
					else if(taskFile.getName().substring(0, taskFile.getName().lastIndexOf(".")).equals(previousFileName))
					{
						uploadFileMap.put(taskFile, new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
						filesToRemove.add(taskFile);
					}
					else
					{
						break;
					}
				}
			}
			//taskFiles.removeAll(filesToRemove);
			
			
			/*for (int i = 0; i < taskFiles.size(); i++)
			{
				if(taskFiles.get(i).getName().toUpperCase().contains(scanIdentifierString)) 
				{
					if(previousFileName == null )
					{
						previousFileName=taskFiles.get(i).getName().substring(0, taskFiles.get(i).getName().lastIndexOf("."));
						uploadFileMap.put(taskFiles.remove(i), new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
					}
					else if(taskFiles.get(i).getName().substring(0, taskFiles.get(i).getName().lastIndexOf(".")).equals(previousFileName))
					{
						uploadFileMap.put(taskFiles.remove(i), new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
					}
					else
					{
						break;
					}
				}
			}*/
			
		}
	}


	/**
	 * Gets the task list.
	 *
	 * @param name the name
	 * @return the task list
	 */
	private List<XnatImagescandataI> getTaskList(String name) {

		List<XnatImagescandataI> list=null;
		if(name.toUpperCase().contains("_EMOTION_"))
			list= emotionList;
		else if(name.toUpperCase().contains("_WM_"))
			list= workingMemList;		
		return list;
	}

}
