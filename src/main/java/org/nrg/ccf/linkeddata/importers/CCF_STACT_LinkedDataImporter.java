/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;



/**
 * The Class CCF_STACT_LinkedDataImporter.
 * 
 * @author Atul
 */
public class CCF_STACT_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_STACT_LinkedDataImporter.class);

	/** The Constant TASK_NAME_EMOTION. */
	private static final String TASK_NAME_EMOTION = "EMOTION";

	/** The Constant TASK_NAME_GAMBLING. */
	private static final String TASK_NAME_GAMBLING = "GAMBLING";

	/** The Constant TASK_NAME_RESTING. */
	private static final String TASK_NAME_RESTING = "RESTING";

	/** The Constant TASK_NAME_WORKING_MEMORY. */
	private static final String TASK_NAME_WORKING_MEMORY = "WORKING_MEMORY";

	/** The Constant TASK_NAME_GUESSING. */
	private static final String TASK_NAME_GUESSING = "GUESSING";

	/** The emotion list. */
	private List<XnatImagescandataI> emotionList=new ArrayList<XnatImagescandataI>();

	/** The gambling list. */
	private List<XnatImagescandataI> gamblingList=new ArrayList<XnatImagescandataI>();

	/** The resting list. */
	private List<XnatImagescandataI> restingList=new ArrayList<XnatImagescandataI>();

	/** The wm list. */
	private List<XnatImagescandataI> wmList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CC F STAC T linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_STACT_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CCF STACT linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_STACT_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(scanLevelDir, null, true);
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split(" ");
			if(sdParts!=null && sdParts.length>1)
			{
				if(TASK_NAME_EMOTION.equalsIgnoreCase(sdParts[1]) && sdParts.length==3)
				{
					emotionList.add(scan);
				}
				else if((TASK_NAME_WORKING_MEMORY.equalsIgnoreCase(sdParts[1]) || "WM".equalsIgnoreCase(sdParts[1])) && sdParts.length==3)
				{
					wmList.add(scan);
				}
				else if((TASK_NAME_RESTING.equalsIgnoreCase(sdParts[0]) || sdParts[0].startsWith("rsFmri")) && sdParts.length==2)
				{
					restingList.add(scan);
				}
				else if(TASK_NAME_GAMBLING.equalsIgnoreCase(sdParts[1]) && sdParts.length==3)
				{
					gamblingList.add(scan);
				}
			}
		}
		LinkedDataUtil.sortTheScanList(emotionList, getReturnList());
		LinkedDataUtil.sortTheScanList(wmList, getReturnList());
		LinkedDataUtil.sortTheScanList(restingList, getReturnList());
		LinkedDataUtil.sortTheScanList(gamblingList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir
	 */
	private void processScanLevelTaskDataFiles(File taskDir) {
		try {
			File[] taskFiles=taskDir.listFiles();
			Arrays.sort(taskFiles);
			for (int i = 0; i < taskFiles.length; i++) {
				List<XnatImagescandataI> scanlist=getTaskList(taskFiles[i].getName());
				if(scanlist!=null && !scanlist.isEmpty())
				{
					XnatImagescandata scan = (XnatImagescandata) scanlist.remove(0);
					uploadFileMap.put(taskFiles[i], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
				}
			}
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
		}
	}


	/**
	 * Gets the task list.
	 *
	 * @param name the name
	 * @return the task list
	 */
	private List<XnatImagescandataI> getTaskList(String name) {

		List<XnatImagescandataI> list=null;
		if(name.toUpperCase().contains(TASK_NAME_EMOTION))
			list= emotionList;
		else if(name.toUpperCase().contains(TASK_NAME_RESTING))
			list= restingList;
		else if(name.toUpperCase().contains(TASK_NAME_GAMBLING) ||name.toUpperCase().contains(TASK_NAME_GUESSING))
			list= gamblingList;
		else if(name.toUpperCase().contains(TASK_NAME_WORKING_MEMORY)|| name.toUpperCase().contains("WORKINGMEM"))
			list= wmList;
		return list;
	}

}
