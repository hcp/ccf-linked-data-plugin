package org.nrg.ccf.linkeddata.importers;

import java.util.Map;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;

/**
 * The Class CCF_HCD_LinkedDataImporter.
 */
public class CCF_HCD_LinkedDataImporter extends CCF_HCA_LinkedDataImporter {
	
	{
		SCAN_PSYCHOPY_TYPES = new String[] { "mbPCASL", "REST", "CARIT", "GUESSING", "EMOTION" };
		SESSION_PSYCHOPY_TYPES = new String[] { "mbPCASL", "REST", "CARIT", "GUESSING", "EMOTION" };
		SPECIAL_HANDLING_TYPES = new String[] { };
				
				
	}
	
	/**
	 * Instantiates a new CC f_ hc d_ linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_HCD_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}
	
	/**
	 * Instantiates a new CC f_ hc d_ linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_HCD_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

}
