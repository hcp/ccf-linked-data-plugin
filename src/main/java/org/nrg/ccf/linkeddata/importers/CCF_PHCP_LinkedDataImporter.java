/**
 * 
 */
package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FileFilter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_PHCP_LinkedDataImporter.
 * @author Atul
 */
public class CCF_PHCP_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_PHCP_LinkedDataImporter.class);

	/** The Constant TASK_NAME_MID. */
	private static final String TASK_NAME_MID = "MID";

	/** The Constant TASK_NAME_COP. */
	private static final String TASK_NAME_COP = "COP";

	/** The Constant TASK_NAME_CSS. */
	private static final String TASK_NAME_CSS = "CSS";

	/** The Constant TASK_NAME_PRF. */
	private static final String TASK_NAME_PRF = "PRF";

	/** The Constant TASK_NAME_DPX. */
	private static final String TASK_NAME_DPX = "DPX";

	/** The Constant TASK_NAME_SOC_COG. */
	private static final String TASK_NAME_SOC_COG = "SOC_COG";
	
	/** The Constant TASK_NAME_SOC_COG. */
	private static final String DIRECTORY_NAME_SOC_COG = "SOCIAL COGNITION";

	/** The mid list. */
	private List<XnatImagescandataI> midList=new ArrayList<XnatImagescandataI>();

	/** The cop list. */
	private List<XnatImagescandataI> copList=new ArrayList<XnatImagescandataI>();

	/** The css list. */
	private List<XnatImagescandataI> cssList=new ArrayList<XnatImagescandataI>();

	/** The prf list. */
	private List<XnatImagescandataI> prfList=new ArrayList<XnatImagescandataI>();

	/** The dpx list. */
	private List<XnatImagescandataI> dpxList=new ArrayList<XnatImagescandataI>();

	/** The soccog list. */
	private List<XnatImagescandataI> soccogList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CC F PHC P linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_PHCP_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CC F PHC P linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_PHCP_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).append(File.separator).append(exp.getLabel()).append(File.separator).append("TaskData").toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(scanLevelDir, null, true);
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split("_");
			if(sdParts!=null && sdParts.length>1)
			{
				if(sdParts[1].contains(TASK_NAME_MID) && sdParts.length==3)
				{
					midList.add(scan);
				}
				else if(sdParts[1].contains(TASK_NAME_PRF) && sdParts.length==3)
				{
					prfList.add(scan);
				}
				else if(sdParts[1].contains(TASK_NAME_CSS) && sdParts.length==4)
				{
					cssList.add(scan);
				}
				else if(sdParts[1].contains(TASK_NAME_COP) && sdParts.length==4)
				{
					copList.add(scan);
				}
				else if(sdParts[1].contains(TASK_NAME_DPX) && sdParts.length==3)
				{
					dpxList.add(scan);
				}
				else if(scan.getSeriesDescription().contains(TASK_NAME_SOC_COG) && sdParts.length==4)
				{
					soccogList.add(scan);
				}
			}
		}
		LinkedDataUtil.sortTheScanList(midList, getReturnList());
		LinkedDataUtil.sortTheScanList(prfList, getReturnList());
		LinkedDataUtil.sortTheScanList(cssList, getReturnList());
		LinkedDataUtil.sortTheScanList(copList, getReturnList());
		LinkedDataUtil.sortTheScanList(dpxList, getReturnList());
		LinkedDataUtil.sortTheScanList(soccogList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir
	 */
	private void processScanLevelTaskDataFiles(File taskDir) {
		try {
			File[] subTaskDirs=taskDir.listFiles();
			Arrays.sort(subTaskDirs);
			List<File> filesAlreadyUsed = new ArrayList<File>();
			for (int i = 0; i < subTaskDirs.length; i++) {
				final File subTaskDir=subTaskDirs[i];
				//logger.debug("Processing scan directory :"+subTaskDir.getName());
				List<XnatImagescandataI> scanlist=getTaskList(subTaskDir.getName());
				if(scanlist!=null)
				{
					Integer runNumber=null;
					Integer listSize=scanlist.size();
					for (int p=0; p< listSize; p++) {
						XnatImagescandata scan = (XnatImagescandata) scanlist.get(p);
						//logger.debug(scan.getSeriesDescription());
						String[] s=scan.getSeriesDescription().split(Constants.UNDERSCORE);
						if(scan.getSeriesDescription().toUpperCase().contains("_TASK"))
						{
							runNumber=new Integer(s[2].substring(4, s[2].length()));//-1;
						}
						else if(scan.getSeriesDescription().toUpperCase().contains("_MID"))
						{
							runNumber=new Integer(s[1].substring(3, s[1].length()));
						}
						else if(scan.getSeriesDescription().toUpperCase().contains("_SOC_COG"))
						{
							runNumber=new Integer(s[2].substring(3, s[2].length()));
						}
						else {
							if(s.length==3)
								runNumber=new Integer(s[1].substring(3, s[1].length()));//-1;
							else if(s.length==4)
								runNumber=new Integer(s[2].substring(3, s[2].length()));//-1;
						}
						final String runNum="RUN"+runNumber;
						final String runNumWithZero="RUN0"+runNumber;
						final String runNumWithExtension=runNumber+".TXT";
						File[] taskFiles= subTaskDir.listFiles(new FileFilter() {
							@Override
							public boolean accept(File file) {
								return /*!file.getName().toUpperCase().contains("PRACTICE") 
										&&*/ (file.getName().toUpperCase().contains(runNum)
												||file.getName().toUpperCase().contains(runNumWithZero)
												||file.getName().toUpperCase().endsWith(runNumWithExtension));
							}
						});
						if("DPX".equals(subTaskDir.getName()))
						{
							File[] dpxFile= subTaskDir.listFiles(new FileFilter() {
								@Override
								public boolean accept(File file) {
									return file.getName().toUpperCase().endsWith("_DPX.TXT");
								}
							});
							if(dpxFile!=null && dpxFile.length > 0 && !filesAlreadyUsed.contains(dpxFile[0]))
							{
								uploadFileMap.put(dpxFile[0], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
								filesAlreadyUsed.add(dpxFile[0]);
							}
						}
						for (int k = 0; k < taskFiles.length; k++) {
							//logger.debug("Added File : "+taskFiles[k].getName());
							uploadFileMap.put(taskFiles[k], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
						}
					}
				}
			}
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
		}
	}


	/**
	 * Gets the task list.
	 *
	 * @param name the name
	 * @return the task list
	 */
	private List<XnatImagescandataI> getTaskList(String name) {

		List<XnatImagescandataI> list=null;
		switch(name.toUpperCase())
		{
		case TASK_NAME_MID:
			list= midList;
			break;
		case TASK_NAME_CSS:
			list= cssList;
			break;
		case TASK_NAME_COP:
			list= copList;
			break;
		case TASK_NAME_PRF:
			list= prfList;
			break;
		case TASK_NAME_DPX:
			list= dpxList;
			break;
		case DIRECTORY_NAME_SOC_COG:
			list= soccogList;
			break;
		}
		return list;
	}

}
