package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_ECP_LinkedDataImporter.
 * 
 * @author Atul
 */
public class CCF_ECP_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_ECP_LinkedDataImporter.class);
	
	/** The Constant SDTONES. */
	private static final String SDTONES = "SDTONES";

	/** The Constant STORYMATH. */
	private static final String STORYMATH = "STORYMATH";

	/** The Constant EMOTION. */
	private static final String EMOTION = "EMOTION";

	/** The Constant SOCIAL. */
	private static final String SOCIAL = "SOCIAL";

	/** The Constant TASK_NAME_SOCIAL. */
	private static final String TASK_NAME_SOCIAL = "SOC";
	
	/** The Constant TASK_NAME_EMOTION. */
	private static final String TASK_NAME_EMOTION = "EMOT";
	
	/** The Constant TASK_NAME_SEMANTIC. */
	private static final String TASK_NAME_SEMANTIC = "SEM";
	
	/** The Constant TASK_NAME_LANGUAGE. */
	private static final String TASK_NAME_LANGUAGE = "LANG";

	/** The soccog list. */
	private List<XnatImagescandataI> soccogList=new ArrayList<XnatImagescandataI>();
	
	/** The emotion list. */
	private List<XnatImagescandataI> emotionList=new ArrayList<XnatImagescandataI>();
	
	/** The language list. */
	private List<XnatImagescandataI> languageList=new ArrayList<XnatImagescandataI>();
	
	/** The semantic list. */
	private List<XnatImagescandataI> semanticList=new ArrayList<XnatImagescandataI>();

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The left over files. */
	List<File> leftOverFiles=new ArrayList<File>();

	/**
	 * Instantiates a new CCF ECP linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_ECP_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw,
			Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CCF ECP linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_ECP_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).toString());
		leftOverFiles =(List<File>) FileUtils.listFiles(scanLevelDir, null, true);
		if (scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.removeAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles, uploadFileMap, returnList);

		LinkedDataUtil.addUploadStatus(leftOverFiles.size() >0?false:upload_status, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}


	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) 
		{
			if(scan.getSeriesDescription().toUpperCase().contains(TASK_NAME_SOCIAL))
			{
				soccogList.add(scan);
			}
			else if(scan.getSeriesDescription().toUpperCase().contains(TASK_NAME_EMOTION))
			{
				emotionList.add(scan);
			}
			else if(scan.getSeriesDescription().toUpperCase().contains(TASK_NAME_SEMANTIC))
			{
				semanticList.add(scan);
			}
			else if(scan.getSeriesDescription().toUpperCase().contains(TASK_NAME_LANGUAGE))
			{
				languageList.add(scan);
			}
		}
		LinkedDataUtil.sortTheScanList(emotionList, getReturnList());
		LinkedDataUtil.sortTheScanList(soccogList, getReturnList());
		LinkedDataUtil.sortTheScanList(semanticList, getReturnList());
		LinkedDataUtil.sortTheScanList(languageList, getReturnList());
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir
	 */
	private void processScanLevelTaskDataFiles(File taskDir) {
		try {
			File[] taskFiles=taskDir.listFiles();
			Arrays.sort(taskFiles);
			for (int i = 0; i < taskFiles.length; i++) {
				List<XnatImagescandataI> scanlist=getTaskList(taskFiles[i].getName());
				Integer fileRunNumber=null;
				if(taskFiles[i].getName().contains("_run"))
				{
					fileRunNumber=new Integer(taskFiles[i].getName().substring(taskFiles[i].getName().indexOf("_run")+4, taskFiles[i].getName().lastIndexOf(".")));
				}
				else
				{
					fileRunNumber=new Integer(taskFiles[i].getName().substring(taskFiles[i].getName().lastIndexOf("-")+1, taskFiles[i].getName().lastIndexOf(".")));
				}
				if(scanlist!=null)
				{
					Integer scanRunNumber=null;
					Integer numberOfScans=scanlist.size();
					for (int p=0; p< numberOfScans; p++) {
						XnatImagescandata scan = (XnatImagescandata) scanlist.get(p);
						if(scan.getSeriesDescription().contains(" PE"))
						{
							scanRunNumber=new Integer(scan.getSeriesDescription().substring(scan.getSeriesDescription().indexOf(" PE")+3, scan.getSeriesDescription().indexOf(" PE")+4));
						}
						if(fileRunNumber.equals(scanRunNumber))
							uploadFileMap.put(taskFiles[i], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
					}
				}
			}
		}catch (Exception e) {
			logger.error("Exception while adding file for upload  "+ExceptionUtils.getStackTrace(e));
		}
	}


	/**
	 * Gets the task list.
	 *
	 * @param name the name
	 * @return the task list
	 */
	private List<XnatImagescandataI> getTaskList(String name) {

		List<XnatImagescandataI> list=null;
		if(name.toUpperCase().contains(SOCIAL))
			list= soccogList;
		else if(name.toUpperCase().contains(EMOTION))
			list= emotionList;
		else if(name.toUpperCase().contains(STORYMATH))
			list= semanticList;
		else if(name.toUpperCase().contains(SDTONES))
			list= languageList;		
		return list;
	}

}
