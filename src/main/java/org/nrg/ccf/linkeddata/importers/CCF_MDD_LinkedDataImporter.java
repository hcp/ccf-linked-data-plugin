package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.DirectoryFilter;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_MDD_LinkedDataImporter.
 */
public class CCF_MDD_LinkedDataImporter extends AbstractLinkedDataImporter {
	
	/** The Constant TASK_NAME_CARIT. */
	private static final String TASK_NAME_CARIT = "CARIT";
	
	/** The Constant TASK_NAME_FACEMATCHING. */
	private static final String TASK_NAME_FACEMATCHING = "FACEMATCHING";
	
	/** The upload file map. */
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();
	
	/** The carit list. */
	private List<XnatImagescandataI> caritList=new ArrayList<XnatImagescandataI>();
	
	/** The face matching list. */
	private List<XnatImagescandataI> faceMatchingList=new ArrayList<XnatImagescandataI>();
	
	
	/**
	 * Instantiates a new CCF MDD linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_MDD_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}
	
	
	/**
	 * Instantiates a new CC F MD D linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_MDD_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	
	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, exp.getLabel()));
		createSortedScanLists(exp.getScans_scan());

		// Start matching EV files to scans
		returnList.add(Constants.PROCESS_SCAN_EV_LEVEL_FILES_MESSAGE);
		File evDir = new File(new StringBuilder(cacheLoc.getAbsolutePath()).append(File.separator).append(exp.getLabel()).append(File.separator).append("EVs").toString());
		if(evDir.exists() && evDir.isDirectory())
			processScanLevelEVFiles(evDir);
		else
			returnList.add(Constants.EVS_DIRECTORY_DOES_NOT_EXISTS);

		
		// Start matching files to session
		returnList.add(Constants.PROCESS_SESSION_LEVEL_FILES_MESSAGE);
		File psychopyDir=new File(new StringBuilder(cacheLoc.getAbsolutePath()).append(File.separator).append(exp.getLabel()).append(File.separator).append("psychopy").toString());
		if(psychopyDir.exists() && psychopyDir.isDirectory())
			processSessionLevelPsychopyFiles(psychopyDir);
		else
			returnList.add(Constants.PSYCHOPY_DIRECTORY_DOES_NOT_EXISTS);
		
		Boolean upload_status=uploadFilesToResources(uploadFileMap);
		
		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		LinkedDataUtil.reportLeftoverFiles(new ArrayList<File>(), uploadFileMap, returnList);
		
		LinkedDataUtil.addUploadStatus(upload_status, returnList);
		
		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE, exp.getLabel()));
	}
	
	/**
	 * Report leftover files.
	 *//*
	private void reportLeftoverFiles() {
		final List<File> keyList = new ArrayList<>();
		keyList.addAll(uploadFileMap.keySet());
		Collections.sort(keyList, new FileNameComparator());
		for (final File f : keyList) {
			if (uploadFileMap.get(f) == null) {
				getReturnList().add("WARNING:  File - <b>" + f.getName() + "</b> - was not uploaded to any scan or to the session.");
			}
		}
	}*/

	/**
	 * Creates the sorted scan lists.
	 *
	 * @param scanList the scan list
	 */
	private void createSortedScanLists(List<XnatImagescandataI> scanList) {
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split("_");
			if(sdParts!=null && sdParts.length==3)
			{
				if(TASK_NAME_CARIT.equalsIgnoreCase(sdParts[1]))
				{
					caritList.add(scan);
				}
				else if(TASK_NAME_FACEMATCHING.equalsIgnoreCase(sdParts[1]))
				{
					faceMatchingList.add(scan);
				}
			}
		}
		
		LinkedDataUtil.sortTheScanList(caritList, getReturnList());
		LinkedDataUtil.sortTheScanList(faceMatchingList, getReturnList());
	}

	/**
	 * Process scan level EV files.
	 *
	 * @param evDir the ev dir
	 * @throws ClientException the client exception
	 */
	private void processScanLevelEVFiles(File evDir) throws ClientException {
		File[] taskDirs=evDir.listFiles(new DirectoryFilter());
		Arrays.sort(taskDirs);
		for (int i = 0; i < taskDirs.length; i++) {
			String dirName=taskDirs[i].getName();
			String[] val=dirName.split("-");
			String taskName=val[1].split("_")[0];
			XnatImagescandataI scan=null;
			if(TASK_NAME_CARIT.equalsIgnoreCase(taskName))
			{
				scan=caritList.remove(0);
			}
			else if("FACE".equalsIgnoreCase(taskName))
			{
				scan=faceMatchingList.remove(0);
			}
			File[] taskFiles=taskDirs[i].listFiles();
			for (int j = 0; j < taskFiles.length; j++) {
				uploadFileMap.put(taskFiles[j], new LinkedFileInfo(scan, Constants.EVS_FOLDER));
			}	
		}
	}
	
	/**
	 * Process session level psychopy files.
	 *
	 * @param psychopyDir the psychopy dir
	 * @throws ClientException the client exception
	 */
	private void processSessionLevelPsychopyFiles(File psychopyDir) throws ClientException {
			File[] psychopyfiles=psychopyDir.listFiles();
			for (int i = 0; i < psychopyfiles.length; i++) {
				uploadFileMap.put(psychopyfiles[i], new LinkedFileInfo(null,  Constants.SESSION_PSYCHOPY_FOLDER));
			}
	}
}
