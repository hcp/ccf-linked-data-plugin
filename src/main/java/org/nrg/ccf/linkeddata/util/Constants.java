/**
 * 
 */
package org.nrg.ccf.linkeddata.util;


/**
 * The Class Constants.
 * @author Atul
 */
public class Constants {
	
	/** The Constant SESSION_PSYCHOPY_FOLDER. */
	public final static String SESSION_PSYCHOPY_FOLDER = "PSYCHOPY";
	
	/** The Constant SCAN_PSYCHOPY_FOLDER. */
	public final static String SCAN_PSYCHOPY_FOLDER = "PSYCHOPY";
	
	/** The Constant EVS_FOLDER. */
	public final static String EVS_FOLDER = "EVs";

	/** The Constant UPLOAD_SESSION_LEVEL_RESOURCES_MESSAGE. */
	public static final String UPLOAD_SESSION_LEVEL_RESOURCES_MESSAGE = "<br><b>UPLOAD SESSION-LEVEL FILES TO SESSION RESOURCES</b>";

	/** The Constant UPLOAD_SCAN_LEVEL_RESOURCES_MESSAGE. */
	public static final String UPLOAD_SCAN_LEVEL_RESOURCES_MESSAGE = "<br><b>UPLOAD SCAN-LEVEL FILES TO SCAN RESOURCES</b>";

	/** The Constant FILE_UPLOAD_ERROR_MESSAGE. */
	public static final String FILE_UPLOAD_ERROR_MESSAGE = "<br><b>ERROR:  Not all files were successfully uploaded.  Please check upload folders carefully.</b>";

	/** The Constant FILE_UPLOAD_SUCCESS_MESSAGE. */
	public static final String FILE_UPLOAD_SUCCESS_MESSAGE = "<br><b>FILE UPLOAD COMPLETE:  All files uploaded successfully.</b>";
	
	/** The Constant SESSION_FILE_UPLOAD_ERROR_MESSAGE. */
	public static final String SESSION_FILE_UPLOAD_ERROR_MESSAGE="ERROR:  Could not upload file <b>{0}</b> to a session resource.";
	
	/** The Constant SCAN_FILE_UPLOAD_ERROR_MESSAGE. */
	public static final String SCAN_FILE_UPLOAD_ERROR_MESSAGE="ERROR:  Could not upload file <b>{0}</b> to a session resource.";
	
	/** The Constant BEGIN_PROCESSING_MESSAGE. */
	public static final String BEGIN_PROCESSING_MESSAGE="<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT =  {0})</b>";
	
	/** The Constant PROCESS_SCAN_LEVEL_EV_FILES_MESSAGE. */
	public static final String PROCESS_SCAN_EV_LEVEL_FILES_MESSAGE="<br><b>PROCESS SCAN-LEVEL EV FILES</b>";
	
	/** The Constant PROCESS_SCAN_LEVEL_FILES_MESSAGE. */
	public static final String PROCESS_SCAN_LEVEL_FILES_MESSAGE="<br><b>PROCESS SCAN-LEVEL FILES</b>";
	
	/** The Constant PROCESS_SESSION_LEVEL_FILES_MESSAGE. */
	public static final String PROCESS_SESSION_LEVEL_FILES_MESSAGE="<br><b>PROCESS SESSION-LEVEL FILES</b>";
	
	/** The Constant PSYCHOPY_DIRECTORY_DOES_NOT_EXISTS. */
	public static final String PSYCHOPY_DIRECTORY_DOES_NOT_EXISTS="<b><i>PSYCHOPY DIRECTORY DOES NOT EXISTS.</i></b>";
	
	/** The Constant EVS_DIRECTORY_DOES_NOT_EXISTS. */
	public static final String EVS_DIRECTORY_DOES_NOT_EXISTS="<b><i>EVs DIRECTORY DOES NOT EXISTS.</i></b>";
	
	/** The Constant TASK_DATA_DIRECTORY_DOES_NOT_EXISTS. */
	public static final String TASK_DATA_DIRECTORY_DOES_NOT_EXISTS="<b><i>TASK DATA DIRECTORY DOES NOT EXISTS.</i></b>";
	
	/** The Constant HCP_MD_PHYSIO_DATA_DIRECTORY_DOES_NOT_EXISTS. */
	public static final String HCP_MD_PHYSIO_DATA_DIRECTORY_DOES_NOT_EXISTS="<b><i>HCP_MD_PHYSIO DATA DIRECTORY DOES NOT EXISTS.</i></b>";
	
	/** The Constant DATA_UPLOAD_COMPLETED_MESSAGE. */
	public static final String DATA_UPLOAD_COMPLETED_MESSAGE="<br><b>LINKED DATA UPLOAD COMPLETE (EXPERIMENT =  {0} )</b>";
	
	public static final String ERROR_FAILED_FILES_MESSAGE="ERROR:  Could not upload files. ( {0} )";
	
	/** The Constant NOTIFY_SCANID_SORT. */
	public static final String NOTIFY_SCANID_SORT = 
			"WARNING:  Could not use scan time ordering for scan comparisons.  Using scan ID ordering. This warrants extra checking of how the files were matched with scans.";
	
	public static final String UNDERSCORE = "_";

}
