/**
 * 
 */
package org.nrg.ccf.linkeddata.util.comparators;

import java.util.Comparator;

import org.nrg.xdat.om.XnatImagesessiondata;

/**
 * The Class sessionIdComparator.
 *
 * @author Atul
 */
public class SessionIdComparator implements Comparator<XnatImagesessiondata>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(XnatImagesessiondata session1, XnatImagesessiondata session2) {
		final String session1id = session1.getId();
		final Integer session1idnum = Integer.valueOf(session1id.replaceAll("\\D", ""));
		final String session1idstr = session1id.replaceAll("\\d", "");
		
		final String session2id = session2.getId();
		final Integer session2idnum = Integer.valueOf(session2id.replaceAll("\\D", ""));
		final String session2idstr = session2id.replaceAll("\\d", "");
		
		final int numCompare = session1idnum.compareTo(session2idnum);
		final int strCompare = session1idstr.compareTo(session2idstr);
		
		return (numCompare!=0) ? numCompare : strCompare;
	}

}
