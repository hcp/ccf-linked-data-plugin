/**
 * 
 */
package org.nrg.ccf.linkeddata.util.comparators;

import java.util.Comparator;

import org.nrg.xdat.model.XnatImagescandataI;

/**
 * The Class ScanIdComparator.
 *
 * @author Atul
 */
public class ScanIdComparator implements Comparator<XnatImagescandataI>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(XnatImagescandataI scan1, XnatImagescandataI scan2) {
		final String scan1id = scan1.getId();
		final Integer scan1idnum = Integer.valueOf(scan1id.replaceAll("\\D", ""));
		final String scan1idstr = scan1id.replaceAll("\\d", "");
		
		final String scan2id = scan2.getId();
		final Integer scan2idnum = Integer.valueOf(scan2id.replaceAll("\\D", ""));
		final String scan2idstr = scan2id.replaceAll("\\d", "");
		
		final int numCompare = scan1idnum.compareTo(scan2idnum);
		final int strCompare = scan1idstr.compareTo(scan2idstr);
		
		return (numCompare!=0) ? numCompare : strCompare;
	}

}
