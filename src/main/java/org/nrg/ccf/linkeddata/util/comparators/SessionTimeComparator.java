/**
 * 
 */
package org.nrg.ccf.linkeddata.util.comparators;

import java.util.Comparator;

import org.nrg.xdat.om.XnatImagesessiondata;


/**
 * The Class ScanTimeComparator.
 *
 * @author Atul
 */
public class SessionTimeComparator implements Comparator<XnatImagesessiondata>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(final XnatImagesessiondata session1,final XnatImagesessiondata session2) {
		
		final Object session1time = session1.getTime();
		final Object session2time = session2.getTime();
		
		// We have some odd physio and secondary scans that don't have a scan time.  We'll just use scanID ordering for these.
		// We won't be placing files in these anyway.
		if (session1time==null || session2time==null) {
			return new SessionIdComparator().compare(session1, session2);
		}
		
		return ((Comparable)session1time).compareTo((Comparable)session2time);
		
	}

}
