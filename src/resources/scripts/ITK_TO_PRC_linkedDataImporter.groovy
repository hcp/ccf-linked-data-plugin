import org.nrg.ccf.linkeddata.importers.ITK_TO_PRC_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

Map<String, Object> map = Maps.newHashMap()
map.put("project", project)
map.put("BuildPath",BuildPath)


def uploader = new ITK_TO_PRC_LinkedDataImporter(user, map)
try {
     uploader.moveCustomUploaderFilesAtProjectLevel('SPECIFY_INTAKE_PROJECT','SPECIFY_PROCESSING_PROJECT');
} catch (Exception e) {
     println e
}