import org.nrg.ccf.linkeddata.importers.ITK_TO_PRC_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

Map<String, Object> map = Maps.newHashMap()
map.put("experiment", experiment)
map.put("project", project)
map.put("BuildPath",BuildPath)

def uploader = new ITK_TO_PRC_LinkedDataImporter(user, map)
try {
	 uploader.moveCustomUploaderFilesAtProjectLevel(project,'MENTION_PROCESSING_PROJECT_ID_HERE');
} catch (Exception e) {
	 println e
}