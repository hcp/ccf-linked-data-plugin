import org.nrg.ccf.linkeddata.importers.CCF_ECP_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

Map<String, Object> map = Maps.newHashMap()
map.put("experiment", experiment)
map.put("BuildPath",BuildPath)
map.put("project", project)


def uploader = new CCF_ECP_LinkedDataImporter(user, map)
try {
     def listout = uploader.call();
     listout.each {
         println it;
     }
     uploader.sendNotifications((user.getEmail() + " akaushal@wustl.edu").split());
} catch (Exception e) {
     println e
     uploader.sendNotifications((user.getEmail() + " akaushal@wustl.edu").split());
}