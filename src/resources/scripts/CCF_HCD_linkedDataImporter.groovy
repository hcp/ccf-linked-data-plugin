import org.nrg.ccf.linkeddata.importers.CCF_HCD_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

//println "user=" + user.getID()
//println "scriptId=" + scriptId.toString()
//println "event=" + event.toString()
//println "srcEventId=" + srcEventId.toString()
//println "srcEventClass=" + srcEventClass.toString()
//println "srcWorkflowId=" + srcWorkflowId.toString()
//println "scriptWorkflowId=" + scriptWorkflowId.toString()
//println "dataType=" + dataType.toString()
//println "dataId=" + dataId.toString()
//println "externalId=" + externalId.toString()
//println "workflow=" + workflow.toString()
//println "arguments=" + arguments.toString()

Map<String, Object> map = Maps.newHashMap()
map.put("experiment", experiment)
map.put("project", project)
map.put("BuildPath",BuildPath)


def uploader = new CCF_HCD_LinkedDataImporter(user, map)
try {
     def listout = uploader.call();
     listout.each {
         println it;
     }
     uploader.sendNotifications((user.getEmail() + " hodgem@wustl.edu gburgess@wustl.edu").split());
} catch (Exception e) {
     println e
     uploader.sendNotifications((user.getEmail() + " hodgem@wustl.edu gburgess@wustl.edu").split());
     //throw e
}