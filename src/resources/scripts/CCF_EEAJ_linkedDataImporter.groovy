import org.nrg.ccf.linkeddata.importers.CCF_EEAJ_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

Map<String, Object> map = Maps.newHashMap()
map.put("experiment", experiment)
map.put("project", project)
map.put("BuildPath",BuildPath)


def uploader = new CCF_EEAJ_LinkedDataImporter(user, map)
try {
     def listout = uploader.call();
     listout.each {
         println it;
     }
     uploader.sendNotifications((user.getEmail() + " akaushal@wustl.edu").split());
} catch (Exception e) {
     println e
     uploader.sendNotifications((user.getEmail() + "  akaushal@wustl.edu").split());
     //throw e
}